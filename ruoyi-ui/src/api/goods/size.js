import request from '@/utils/request'

// 查询尺寸列表
export function listSize(query) {
  return request({
    url: '/goods/size/list',
    method: 'get',
    params: query
  })
}

// 查询尺寸详细
export function getSize(sizeId) {
  return request({
    url: '/goods/size/' + sizeId,
    method: 'get'
  })
}

// 新增尺寸
export function addSize(data) {
  return request({
    url: '/goods/size',
    method: 'post',
    data: data
  })
}

// 修改尺寸
export function updateSize(data) {
  return request({
    url: '/goods/size',
    method: 'put',
    data: data
  })
}

// 删除尺寸
export function delSize(sizeId) {
  return request({
    url: '/goods/size/' + sizeId,
    method: 'delete'
  })
}

// 导出尺寸
export function exportSize(query) {
  return request({
    url: '/goods/size/export',
    method: 'get',
    params: query
  })
}