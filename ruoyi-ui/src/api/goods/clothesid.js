import request from '@/utils/request'

// 查询衣服关系列表
export function listClothesid(query) {
  return request({
    url: '/goods/clothesid/list',
    method: 'get',
    params: query
  })
}

// 查询衣服关系详细
export function getClothesid(clothId) {
  return request({
    url: '/goods/clothesid/' + clothId,
    method: 'get'
  })
}

// 新增衣服关系
export function addClothesid(data) {
  return request({
    url: '/goods/clothesid',
    method: 'post',
    data: data
  })
}

// 修改衣服关系
export function updateClothesid(data) {
  return request({
    url: '/goods/clothesid',
    method: 'put',
    data: data
  })
}

// 删除衣服关系
export function delClothesid(clothId) {
  return request({
    url: '/goods/clothesid/' + clothId,
    method: 'delete'
  })
}

// 导出衣服关系
export function exportClothesid(query) {
  return request({
    url: '/goods/clothesid/export',
    method: 'get',
    params: query
  })
}