import request from '@/utils/request'

// 查询衣服颜色列表
export function listColor(query) {
  return request({
    url: '/goods/color/list',
    method: 'get',
    params: query
  })
}

// 查询衣服颜色详细
export function getColor(colorId) {
  return request({
    url: '/goods/color/' + colorId,
    method: 'get'
  })
}

// 新增衣服颜色
export function addColor(data) {
  return request({
    url: '/goods/color',
    method: 'post',
    data: data
  })
}

// 修改衣服颜色
export function updateColor(data) {
  return request({
    url: '/goods/color',
    method: 'put',
    data: data
  })
}

// 删除衣服颜色
export function delColor(colorId) {
  return request({
    url: '/goods/color/' + colorId,
    method: 'delete'
  })
}

// 导出衣服颜色
export function exportColor(query) {
  return request({
    url: '/goods/color/export',
    method: 'get',
    params: query
  })
}