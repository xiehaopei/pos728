import request from '@/utils/request'

// 查询衣服商品列表
export function listClothes(query) {
  return request({
    url: '/goods/clothes/list',
    method: 'get',
    params: query
  })
}

// 查询衣服商品详细
export function getClothes(id) {
  return request({
    url: '/goods/clothes/' + id,
    method: 'get'
  })
}

// 新增衣服商品
export function addClothes(data) {
  return request({
    url: '/goods/clothes',
    method: 'post',
    data: data
  })
}

// 修改衣服商品
export function updateClothes(data) {
  return request({
    url: '/goods/clothes',
    method: 'put',
    data: data
  })
}

// 删除衣服商品
export function delClothes(id) {
  return request({
    url: '/goods/clothes/' + id,
    method: 'delete'
  })
}

// 导出衣服商品
export function exportClothes(query) {
  return request({
    url: '/goods/clothes/export',
    method: 'get',
    params: query
  })
}