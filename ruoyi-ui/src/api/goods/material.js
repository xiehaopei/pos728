import request from '@/utils/request'

// 查询衣服原料列表
export function listMaterial(query) {
  return request({
    url: '/goods/material/list',
    method: 'get',
    params: query
  })
}

// 查询衣服原料详细
export function getMaterial(materialId) {
  return request({
    url: '/goods/material/' + materialId,
    method: 'get'
  })
}

// 新增衣服原料
export function addMaterial(data) {
  return request({
    url: '/goods/material',
    method: 'post',
    data: data
  })
}

// 修改衣服原料
export function updateMaterial(data) {
  return request({
    url: '/goods/material',
    method: 'put',
    data: data
  })
}

// 删除衣服原料
export function delMaterial(materialId) {
  return request({
    url: '/goods/material/' + materialId,
    method: 'delete'
  })
}

// 导出衣服原料
export function exportMaterial(query) {
  return request({
    url: '/goods/material/export',
    method: 'get',
    params: query
  })
}