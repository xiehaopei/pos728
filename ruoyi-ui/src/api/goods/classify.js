// 服装分类API
import request from '@/utils/request'

// 查询衣服分类列表
export function listClassify(query) {
  return request({
    url: '/goods/classify/list',
    method: 'get',
    params: query
  })
}

// 查询衣服分类详细
export function getClassify(classifyId) {
  return request({
    url: '/goods/classify/' + classifyId,
    method: 'get'
  })
}

// 新增衣服分类
export function addClassify(data) {
  return request({
    url: '/goods/classify',
    method: 'post',
    data: data
  })
}

// 修改衣服分类
export function updateClassify(data) {
  return request({
    url: '/goods/classify',
    method: 'put',
    data: data
  })
}

// 删除衣服分类
export function delClassify(classifyId) {
  return request({
    url: '/goods/classify/' + classifyId,
    method: 'delete'
  })
}

// 导出衣服分类
export function exportClassify(query) {
  return request({
    url: '/goods/classify/export',
    method: 'get',
    params: query
  })
}