import request from '@/utils/request'

// 查询订单支付列表
export function listPayment(query) {
  return request({
    url: '/orders/payment/list',
    method: 'get',
    params: query
  })
}

// 查询订单支付详细
export function getPayment(id) {
  return request({
    url: '/orders/payment/' + id,
    method: 'get'
  })
}

// 新增订单支付
export function addPayment(data) {
  return request({
    url: '/orders/payment',
    method: 'post',
    data: data
  })
}

// 修改订单支付
export function updatePayment(data) {
  return request({
    url: '/orders/payment',
    method: 'put',
    data: data
  })
}

// 删除订单支付
export function delPayment(id) {
  return request({
    url: '/orders/payment/' + id,
    method: 'delete'
  })
}

// 导出订单支付
export function exportPayment(query) {
  return request({
    url: '/orders/payment/export',
    method: 'get',
    params: query
  })
}