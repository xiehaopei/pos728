import request from '@/utils/request'

// 查询订单信息列表
export function listOrder(query) {
  return request({
    url: '/orders/order/list',
    method: 'get',
    params: query
  })
}

// 查询订单信息详细
export function getOrder(ordId) {
  return request({
    url: '/orders/order/' + ordId,
    method: 'get'
  })
}

// 新增订单信息
export function addOrder(data) {
  return request({
    url: '/orders/order',
    method: 'post',
    data: data
  })
}

// 修改订单信息
export function updateOrder(data) {
  return request({
    url: '/orders/order',
    method: 'put',
    data: data
  })
}

// 删除订单信息
export function delOrder(ordId) {
  return request({
    url: '/orders/order/' + ordId,
    method: 'delete'
  })
}

// 导出订单信息
export function exportOrder(query) {
  return request({
    url: '/orders/order/export',
    method: 'get',
    params: query
  })
}