// 商铺管理API
import request from '@/utils/request'

// 查询商铺信息列表
export function listShops(query) {
  return request({
    url: '/stores/shops/list',
    method: 'get',
    params: query
  })
}

// 查询商铺信息详细
export function getShops(shopId) {
  return request({
    url: '/stores/shops/' + shopId,
    method: 'get'
  })
}

// 新增商铺信息
export function addShops(data) {
  return request({
    url: '/stores/shops',
    method: 'post',
    data: data
  })
}

// 修改商铺信息
export function updateShops(data) {
  return request({
    url: '/stores/shops',
    method: 'put',
    data: data
  })
}

// 删除商铺信息
export function delShops(shopId) {
  return request({
    url: '/stores/shops/' + shopId,
    method: 'delete'
  })
}

// 导出商铺信息
export function exportShops(query) {
  return request({
    url: '/stores/shops/export',
    method: 'get',
    params: query
  })
}