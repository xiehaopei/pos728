// 职工管理API
import request from '@/utils/request'

// 查询服装店工作人员列表
export function listStaff(query) {
  return request({
    url: '/stores/staff/list',
    method: 'get',
    params: query
  })
}

// 查询服装店工作人员详细
export function getStaff(sId) {
  return request({
    url: '/stores/staff/' + sId,
    method: 'get'
  })
}

// 新增服装店工作人员
export function addStaff(data) {
  return request({
    url: '/stores/staff',
    method: 'post',
    data: data
  })
}

// 修改服装店工作人员
export function updateStaff(data) {
  return request({
    url: '/stores/staff',
    method: 'put',
    data: data
  })
}

// 删除服装店工作人员
export function delStaff(sId) {
  return request({
    url: '/stores/staff/' + sId,
    method: 'delete'
  })
}

// 导出服装店工作人员
export function exportStaff(query) {
  return request({
    url: '/stores/staff/export',
    method: 'get',
    params: query
  })
}