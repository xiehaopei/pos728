import request from '@/utils/request'

// 查询衣服库存列表
export function listClothesnum(query) {
  return request({
    url: '/stores/clothesnum/list',
    method: 'get',
    params: query
  })
}

// 查询衣服库存详细
export function getClothesnum(id) {
  return request({
    url: '/stores/clothesnum/' + id,
    method: 'get'
  })
}

// 新增衣服库存
export function addClothesnum(data) {
  return request({
    url: '/stores/clothesnum',
    method: 'post',
    data: data
  })
}

// 修改衣服库存
export function updateClothesnum(data) {
  return request({
    url: '/stores/clothesnum',
    method: 'put',
    data: data
  })
}

// 删除衣服库存
export function delClothesnum(id) {
  return request({
    url: '/stores/clothesnum/' + id,
    method: 'delete'
  })
}

// 导出衣服库存
export function exportClothesnum(query) {
  return request({
    url: '/stores/clothesnum/export',
    method: 'get',
    params: query
  })
}