// 库存管理API
import request from '@/utils/request'

// 查询衣服仓库列表
export function listWarehouse(query) {
  return request({
    url: '/stores/warehouse/list',
    method: 'get',
    params: query
  })
}

// 查询衣服仓库详细
export function getWarehouse(warehouseId) {
  return request({
    url: '/stores/warehouse/' + warehouseId,
    method: 'get'
  })
}

// 新增衣服仓库
export function addWarehouse(data) {
  return request({
    url: '/stores/warehouse',
    method: 'post',
    data: data
  })
}

// 修改衣服仓库
export function updateWarehouse(data) {
  return request({
    url: '/stores/warehouse',
    method: 'put',
    data: data
  })
}

// 删除衣服仓库
export function delWarehouse(warehouseId) {
  return request({
    url: '/stores/warehouse/' + warehouseId,
    method: 'delete'
  })
}

// 导出衣服仓库
export function exportWarehouse(query) {
  return request({
    url: '/stores/warehouse/export',
    method: 'get',
    params: query
  })
}