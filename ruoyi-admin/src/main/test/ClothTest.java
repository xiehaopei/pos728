import com.ruoyi.RuoYiApplication;
import com.ruoyi.pos.domain.TbOrder;
import com.ruoyi.pos.domain.TbPayment;
import com.ruoyi.pos.service.impl.TbClothesServiceImpl;
import com.ruoyi.pos.service.impl.TbOrderServiceImpl;
import com.ruoyi.pos.service.impl.TbPaymentServiceImpl;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RuoYiApplication.class)
public class ClothTest {
    @Autowired
    TbClothesServiceImpl clothesService;

    @Autowired
    TbOrderServiceImpl orderService;

    @Autowired
    TbPaymentServiceImpl paymentService;

    @Test
    public void order() {
        TbOrder tbOrder = new TbOrder();
        List<TbOrder> list = orderService.selectTbOrderList(tbOrder);
        Map<Long, Object> hashMap = new HashMap<>();
        for (int i = 0; i <list.size(); i++) {

            TbOrder temp = list.get(i);
            hashMap.put(temp.getOrdId(), temp);
        }
        System.out.println(list);
    }
    @Test
    public void insertData() {


        TbPayment tbPayment = new TbPayment();
        tbPayment.setPayment(new BigDecimal(422));
        tbPayment.setPayTime(new Date());
        tbPayment.setOrdId(131L);
        tbPayment.setPayMethod("银行卡");
        tbPayment.setPayPath("zhifu");

        //
        
        System.out.println(paymentService.insertTbPayment(tbPayment));
    }
    @org.junit.Test
    public void test() {
        System.out.println(clothesService.selectTbClothesById(4L));
    }
}
