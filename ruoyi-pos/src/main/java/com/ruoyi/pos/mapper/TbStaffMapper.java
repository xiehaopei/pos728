package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbStaff;

/**
 * 服装店工作人员Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public interface TbStaffMapper 
{
    /**
     * 查询服装店工作人员
     * 
     * @param sId 服装店工作人员ID
     * @return 服装店工作人员
     */
    public TbStaff selectTbStaffById(String sId);

    /**
     * 查询服装店工作人员列表
     * 
     * @param tbStaff 服装店工作人员
     * @return 服装店工作人员集合
     */
    public List<TbStaff> selectTbStaffList(TbStaff tbStaff);

    /**
     * 新增服装店工作人员
     * 
     * @param tbStaff 服装店工作人员
     * @return 结果
     */
    public int insertTbStaff(TbStaff tbStaff);

    /**
     * 修改服装店工作人员
     * 
     * @param tbStaff 服装店工作人员
     * @return 结果
     */
    public int updateTbStaff(TbStaff tbStaff);

    /**
     * 删除服装店工作人员
     * 
     * @param sId 服装店工作人员ID
     * @return 结果
     */
    public int deleteTbStaffById(String sId);

    /**
     * 批量删除服装店工作人员
     * 
     * @param sIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbStaffByIds(String[] sIds);
}
