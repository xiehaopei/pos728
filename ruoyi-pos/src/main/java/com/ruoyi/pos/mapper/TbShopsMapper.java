package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbShops;

/**
 * 商铺信息Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public interface TbShopsMapper 
{
    /**
     * 查询商铺信息
     * 
     * @param shopId 商铺信息ID
     * @return 商铺信息
     */
    public TbShops selectTbShopsById(String shopId);

    /**
     * 查询商铺信息列表
     * 
     * @param tbShops 商铺信息
     * @return 商铺信息集合
     */
    public List<TbShops> selectTbShopsList(TbShops tbShops);

    /**
     * 新增商铺信息
     * 
     * @param tbShops 商铺信息
     * @return 结果
     */
    public int insertTbShops(TbShops tbShops);

    /**
     * 修改商铺信息
     * 
     * @param tbShops 商铺信息
     * @return 结果
     */
    public int updateTbShops(TbShops tbShops);

    /**
     * 删除商铺信息
     * 
     * @param shopId 商铺信息ID
     * @return 结果
     */
    public int deleteTbShopsById(String shopId);

    /**
     * 批量删除商铺信息
     * 
     * @param shopIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbShopsByIds(String[] shopIds);
}
