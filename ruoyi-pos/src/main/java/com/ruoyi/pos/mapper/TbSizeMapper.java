package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbSize;

/**
 * 尺寸Mapper接口
 * 
 * @author ruoyi
<<<<<<< HEAD
 * @date 2021-05-31
=======
 * @date 2021-06-01
>>>>>>> service
 */
public interface TbSizeMapper 
{
    /**
     * 查询尺寸
     * 
     * @param sizeId 尺寸ID
     * @return 尺寸
     */
    public TbSize selectTbSizeById(Long sizeId);

    /**
     * 查询尺寸列表
     * 
     * @param tbSize 尺寸
     * @return 尺寸集合
     */
    public List<TbSize> selectTbSizeList(TbSize tbSize);

    /**
     * 新增尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    public int insertTbSize(TbSize tbSize);

    /**
     * 修改尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    public int updateTbSize(TbSize tbSize);

    /**
     * 删除尺寸
     * 
     * @param sizeId 尺寸ID
     * @return 结果
     */
    public int deleteTbSizeById(Long sizeId);

    /**
     * 批量删除尺寸
     * 
     * @param sizeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbSizeByIds(Long[] sizeIds);
}
