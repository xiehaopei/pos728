package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbReceivables;

/**
 * 收款状况



Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public interface TbReceivablesMapper 
{
    /**
     * 查询收款状况




     * 
     * @param id 收款状况



ID
     * @return 收款状况




     */
    public TbReceivables selectTbReceivablesById(Long id);

    /**
     * 查询收款状况



列表
     * 
     * @param tbReceivables 收款状况




     * @return 收款状况



集合
     */
    public List<TbReceivables> selectTbReceivablesList(TbReceivables tbReceivables);

    /**
     * 新增收款状况




     * 
     * @param tbReceivables 收款状况




     * @return 结果
     */
    public int insertTbReceivables(TbReceivables tbReceivables);

    /**
     * 修改收款状况




     * 
     * @param tbReceivables 收款状况




     * @return 结果
     */
    public int updateTbReceivables(TbReceivables tbReceivables);

    /**
     * 删除收款状况




     * 
     * @param id 收款状况



ID
     * @return 结果
     */
    public int deleteTbReceivablesById(Long id);

    /**
     * 批量删除收款状况




     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbReceivablesByIds(Long[] ids);
}
