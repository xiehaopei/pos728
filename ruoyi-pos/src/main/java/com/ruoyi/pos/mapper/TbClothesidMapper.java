package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbClothesid;

/**
 * 衣服关系
Mapper接口
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
public interface TbClothesidMapper 
{
    /**
     * 查询衣服关系

     * 
     * @param clothId 衣服关系
ID
     * @return 衣服关系

     */
    public TbClothesid selectTbClothesidById(Long clothId);

    /**
     * 查询衣服关系
列表
     * 
     * @param tbClothesid 衣服关系

     * @return 衣服关系
集合
     */
    public List<TbClothesid> selectTbClothesidList(TbClothesid tbClothesid);

    /**
     * 新增衣服关系

     * 
     * @param tbClothesid 衣服关系

     * @return 结果
     */
    public int insertTbClothesid(TbClothesid tbClothesid);

    /**
     * 修改衣服关系

     * 
     * @param tbClothesid 衣服关系

     * @return 结果
     */
    public int updateTbClothesid(TbClothesid tbClothesid);

    /**
     * 删除衣服关系

     * 
     * @param clothId 衣服关系
ID
     * @return 结果
     */
    public int deleteTbClothesidById(Long clothId);

    /**
     * 批量删除衣服关系

     * 
     * @param clothIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbClothesidByIds(Long[] clothIds);
}
