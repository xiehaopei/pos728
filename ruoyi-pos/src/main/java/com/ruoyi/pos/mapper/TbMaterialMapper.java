package com.ruoyi.pos.mapper;

import java.util.List;
import com.ruoyi.pos.domain.TbMaterial;

/**
 * 衣服原料
Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
public interface TbMaterialMapper 
{
    /**
     * 查询衣服原料

     * 
     * @param materialId 衣服原料
ID
     * @return 衣服原料

     */
    public TbMaterial selectTbMaterialById(Long materialId);

    /**
     * 查询衣服原料
列表
     * 
     * @param tbMaterial 衣服原料

     * @return 衣服原料
集合
     */
    public List<TbMaterial> selectTbMaterialList(TbMaterial tbMaterial);

    /**
     * 新增衣服原料

     * 
     * @param tbMaterial 衣服原料

     * @return 结果
     */
    public int insertTbMaterial(TbMaterial tbMaterial);

    /**
     * 修改衣服原料

     * 
     * @param tbMaterial 衣服原料

     * @return 结果
     */
    public int updateTbMaterial(TbMaterial tbMaterial);

    /**
     * 删除衣服原料

     * 
     * @param materialId 衣服原料
ID
     * @return 结果
     */
    public int deleteTbMaterialById(Long materialId);

    /**
     * 批量删除衣服原料

     * 
     * @param materialIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTbMaterialByIds(Long[] materialIds);
}
