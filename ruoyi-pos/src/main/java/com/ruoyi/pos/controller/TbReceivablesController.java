package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbReceivables;
import com.ruoyi.pos.service.ITbReceivablesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收款状况



Controller
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@RestController
@RequestMapping("/orders/receivables")
public class TbReceivablesController extends BaseController
{
    @Autowired
    private ITbReceivablesService tbReceivablesService;

    /**
     * 查询收款状况



列表
     */
   // @PreAuthorize("@ss.hasPermi('orders:receivables:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbReceivables tbReceivables)
    {
        startPage();
        List<TbReceivables> list = tbReceivablesService.selectTbReceivablesList(tbReceivables);
        return getDataTable(list);
    }

    /**
     * 导出收款状况



列表
     */
    @PreAuthorize("@ss.hasPermi('orders:receivables:export')")
    @Log(title = "收款状况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbReceivables tbReceivables)
    {
        List<TbReceivables> list = tbReceivablesService.selectTbReceivablesList(tbReceivables);
        ExcelUtil<TbReceivables> util = new ExcelUtil<TbReceivables>(TbReceivables.class);
        return util.exportExcel(list, "receivables");
    }

    /**
     * 获取收款状况



详细信息
     */
    @PreAuthorize("@ss.hasPermi('orders:receivables:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tbReceivablesService.selectTbReceivablesById(id));
    }

    /**
     * 新增收款状况




     */
    @PreAuthorize("@ss.hasPermi('orders:receivables:add')")
    @Log(title = "收款状况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbReceivables tbReceivables)
    {
        return toAjax(tbReceivablesService.insertTbReceivables(tbReceivables));
    }

    /**
     * 修改收款状况




     */
    @PreAuthorize("@ss.hasPermi('orders:receivables:edit')")
    @Log(title = "收款状况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbReceivables tbReceivables)
    {
        return toAjax(tbReceivablesService.updateTbReceivables(tbReceivables));
    }

    /**
     * 删除收款状况




     */
    @PreAuthorize("@ss.hasPermi('orders:receivables:remove')")
    @Log(title = "收款状况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tbReceivablesService.deleteTbReceivablesByIds(ids));
    }
}
