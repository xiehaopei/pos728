package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbWarehouse;
import com.ruoyi.pos.service.ITbWarehouseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服仓库Controller
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@RestController
@RequestMapping("/stores/warehouse")
public class TbWarehouseController extends BaseController
{
    @Autowired
    private ITbWarehouseService tbWarehouseService;

    /**
     * 查询衣服仓库列表
     */
 //   @PreAuthorize("@ss.hasPermi('stores:warehouse:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbWarehouse tbWarehouse)
    {
        startPage();
        List<TbWarehouse> list = tbWarehouseService.selectTbWarehouseList(tbWarehouse);
        return getDataTable(list);
    }

    /**
     * 导出衣服仓库列表
     */
    @PreAuthorize("@ss.hasPermi('stores:warehouse:export')")
    @Log(title = "衣服仓库", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbWarehouse tbWarehouse)
    {
        List<TbWarehouse> list = tbWarehouseService.selectTbWarehouseList(tbWarehouse);
        ExcelUtil<TbWarehouse> util = new ExcelUtil<TbWarehouse>(TbWarehouse.class);
        return util.exportExcel(list, "warehouse");
    }

    /**
     * 获取衣服仓库详细信息
     */
    @PreAuthorize("@ss.hasPermi('stores:warehouse:query')")
    @GetMapping(value = "/{warehouseId}")
    public AjaxResult getInfo(@PathVariable("warehouseId") String warehouseId)
    {
        return AjaxResult.success(tbWarehouseService.selectTbWarehouseById(warehouseId));
    }

    /**
     * 新增衣服仓库
     */
    @PreAuthorize("@ss.hasPermi('stores:warehouse:add')")
    @Log(title = "衣服仓库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbWarehouse tbWarehouse)
    {
        return toAjax(tbWarehouseService.insertTbWarehouse(tbWarehouse));
    }

    /**
     * 修改衣服仓库
     */
    @PreAuthorize("@ss.hasPermi('stores:warehouse:edit')")
    @Log(title = "衣服仓库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbWarehouse tbWarehouse)
    {
        return toAjax(tbWarehouseService.updateTbWarehouse(tbWarehouse));
    }

    /**
     * 删除衣服仓库
     */
    @PreAuthorize("@ss.hasPermi('stores:warehouse:remove')")
    @Log(title = "衣服仓库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{warehouseIds}")
    public AjaxResult remove(@PathVariable String[] warehouseIds)
    {
        return toAjax(tbWarehouseService.deleteTbWarehouseByIds(warehouseIds));
    }
}
