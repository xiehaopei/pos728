package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbClothes;
import com.ruoyi.pos.service.ITbClothesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服商品






Controller
 * 
 * @author ruoyi
 * @date 2021-06-04
 */
@RestController
@RequestMapping("/goods/clothes")
public class TbClothesController extends BaseController
{
    @Autowired
    private ITbClothesService tbClothesService;

    /**
     * 查询衣服商品






列表
     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbClothes tbClothes)
    {
        startPage();
        List<TbClothes> list = tbClothesService.selectTbClothesList(tbClothes);
        return getDataTable(list);
    }

    /**
     * 导出衣服商品






列表
     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:export')")
    @Log(title = "衣服商品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbClothes tbClothes)
    {
        List<TbClothes> list = tbClothesService.selectTbClothesList(tbClothes);
        ExcelUtil<TbClothes> util = new ExcelUtil<TbClothes>(TbClothes.class);
        return util.exportExcel(list, "clothes");
    }

    /**
     * 获取衣服商品






详细信息
     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tbClothesService.selectTbClothesById(id));
    }

    /**
     * 新增衣服商品







     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:add')")
    @Log(title = "衣服商品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbClothes tbClothes)
    {
        return toAjax(tbClothesService.insertTbClothes(tbClothes));
    }

    /**
     * 修改衣服商品







     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:edit')")
    @Log(title = "衣服商品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbClothes tbClothes)
    {
        return toAjax(tbClothesService.updateTbClothes(tbClothes));
    }

    /**
     * 删除衣服商品







     */
//    @PreAuthorize("@ss.hasPermi('goods:clothes:remove')")
    @Log(title = "衣服商品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tbClothesService.deleteTbClothesByIds(ids));
    }
}
