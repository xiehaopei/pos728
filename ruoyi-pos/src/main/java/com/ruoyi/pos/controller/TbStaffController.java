package com.ruoyi.pos.controller;

import java.util.List;


import com.ruoyi.pos.domain.TbStaff;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.service.ITbStaffService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 服装店工作人员Controller
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@RestController
@RequestMapping("/stores/staff")
public class TbStaffController extends BaseController
{
    @Autowired
    private ITbStaffService tbStaffService;

    /**
     * 查询服装店工作人员列表
     */
 //   @PreAuthorize("@ss.hasPermi('stores:staff:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbStaff tbStaff)
    {
        startPage();
        List<TbStaff> list = tbStaffService.selectTbStaffList(tbStaff);
        return getDataTable(list);
    }

    /**
     * 导出服装店工作人员列表
     */
    @PreAuthorize("@ss.hasPermi('stores:staff:export')")
    @Log(title = "服装店工作人员", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbStaff tbStaff)
    {
        List<TbStaff> list = tbStaffService.selectTbStaffList(tbStaff);
        ExcelUtil<TbStaff> util = new ExcelUtil<TbStaff>(TbStaff.class);
        return util.exportExcel(list, "staff");
    }

    /**
     * 获取服装店工作人员详细信息
     */
    @PreAuthorize("@ss.hasPermi('stores:staff:query')")
    @GetMapping(value = "/{sId}")
    public AjaxResult getInfo(@PathVariable("sId") String sId)
    {
        return AjaxResult.success(tbStaffService.selectTbStaffById(sId));
    }

    /**
     * 新增服装店工作人员
     */
    @PreAuthorize("@ss.hasPermi('stores:staff:add')")
    @Log(title = "服装店工作人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbStaff tbStaff)
    {
        return toAjax(tbStaffService.insertTbStaff(tbStaff));
    }

    /**
     * 修改服装店工作人员
     */
    @PreAuthorize("@ss.hasPermi('stores:staff:edit')")
    @Log(title = "服装店工作人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbStaff tbStaff)
    {
        return toAjax(tbStaffService.updateTbStaff(tbStaff));
    }

    /**
     * 删除服装店工作人员
     */
    @PreAuthorize("@ss.hasPermi('stores:staff:remove')")
    @Log(title = "服装店工作人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{sIds}")
    public AjaxResult remove(@PathVariable String[] sIds)
    {
        return toAjax(tbStaffService.deleteTbStaffByIds(sIds));
    }
}
