package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbClothesid;
import com.ruoyi.pos.service.ITbClothesidService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服关系
Controller
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/goods/clothesid")
public class TbClothesidController extends BaseController
{
    @Autowired
    private ITbClothesidService tbClothesidService;

    /**
     * 查询衣服关系
列表
     */
   // @PreAuthorize("@ss.hasPermi('goods:clothesid:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbClothesid tbClothesid)
    {
        startPage();
        List<TbClothesid> list = tbClothesidService.selectTbClothesidList(tbClothesid);
        return getDataTable(list);
    }

    /**
     * 导出衣服关系
列表
     */
    @PreAuthorize("@ss.hasPermi('goods:clothesid:export')")
    @Log(title = "衣服关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbClothesid tbClothesid)
    {
        List<TbClothesid> list = tbClothesidService.selectTbClothesidList(tbClothesid);
        ExcelUtil<TbClothesid> util = new ExcelUtil<TbClothesid>(TbClothesid.class);
        return util.exportExcel(list, "clothesid");
    }

    /**
     * 获取衣服关系
详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:clothesid:query')")
    @GetMapping(value = "/{clothId}")
    public AjaxResult getInfo(@PathVariable("clothId") Long clothId)
    {
        return AjaxResult.success(tbClothesidService.selectTbClothesidById(clothId));
    }

    /**
     * 新增衣服关系

     */
    @PreAuthorize("@ss.hasPermi('goods:clothesid:add')")
    @Log(title = "衣服关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbClothesid tbClothesid)
    {
        return toAjax(tbClothesidService.insertTbClothesid(tbClothesid));
    }

    /**
     * 修改衣服关系

     */
    @PreAuthorize("@ss.hasPermi('goods:clothesid:edit')")
    @Log(title = "衣服关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbClothesid tbClothesid)
    {
        return toAjax(tbClothesidService.updateTbClothesid(tbClothesid));
    }

    /**
     * 删除衣服关系

     */
    @PreAuthorize("@ss.hasPermi('goods:clothesid:remove')")
    @Log(title = "衣服关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{clothIds}")
    public AjaxResult remove(@PathVariable Long[] clothIds)
    {
        return toAjax(tbClothesidService.deleteTbClothesidByIds(clothIds));
    }
}
