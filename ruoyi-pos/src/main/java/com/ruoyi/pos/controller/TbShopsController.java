package com.ruoyi.pos.controller;

import java.util.List;

import com.ruoyi.pos.domain.TbShops;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.service.ITbShopsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商铺信息Controller
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@RestController
@RequestMapping("/stores/shops")
public class TbShopsController extends BaseController
{
    @Autowired
    private ITbShopsService tbShopsService;

    /**
     * 查询商铺信息列表
     */
//    @PreAuthorize("@ss.hasPermi('stores:shops:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbShops tbShops)
    {
        startPage();
        List<TbShops> list = tbShopsService.selectTbShopsList(tbShops);
        return getDataTable(list);
    }

    /**
     * 导出商铺信息列表
     */
    @PreAuthorize("@ss.hasPermi('stores:shops:export')")
    @Log(title = "商铺信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbShops tbShops)
    {
        List<TbShops> list = tbShopsService.selectTbShopsList(tbShops);
        ExcelUtil<TbShops> util = new ExcelUtil<TbShops>(TbShops.class);
        return util.exportExcel(list, "shops");
    }

    /**
     * 获取商铺信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('stores:shops:query')")
    @GetMapping(value = "/{shopId}")
    public AjaxResult getInfo(@PathVariable("shopId") String shopId)
    {
        return AjaxResult.success(tbShopsService.selectTbShopsById(shopId));
    }

    /**
     * 新增商铺信息
     */
    @PreAuthorize("@ss.hasPermi('stores:shops:add')")
    @Log(title = "商铺信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbShops tbShops)
    {
        return toAjax(tbShopsService.insertTbShops(tbShops));
    }

    /**
     * 修改商铺信息
     */
    @PreAuthorize("@ss.hasPermi('stores:shops:edit')")
    @Log(title = "商铺信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbShops tbShops)
    {
        return toAjax(tbShopsService.updateTbShops(tbShops));
    }

    /**
     * 删除商铺信息
     */
    @PreAuthorize("@ss.hasPermi('stores:shops:remove')")
    @Log(title = "商铺信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{shopIds}")
    public AjaxResult remove(@PathVariable String[] shopIds)
    {
        return toAjax(tbShopsService.deleteTbShopsByIds(shopIds));
    }
}
