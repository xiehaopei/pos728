package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbMaterial;
import com.ruoyi.pos.service.ITbMaterialService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服原料
Controller
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
@RestController
@RequestMapping("/goods/material")
public class TbMaterialController extends BaseController
{
    @Autowired
    private ITbMaterialService tbMaterialService;

    /**
     * 查询衣服原料
列表
     */
    //@PreAuthorize("@ss.hasPermi('goods:material:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbMaterial tbMaterial)
    {
        startPage();
        List<TbMaterial> list = tbMaterialService.selectTbMaterialList(tbMaterial);
        return getDataTable(list);
    }

    /**
     * 导出衣服原料
列表
     */
    @PreAuthorize("@ss.hasPermi('goods:material:export')")
    @Log(title = "衣服原料 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbMaterial tbMaterial)
    {
        List<TbMaterial> list = tbMaterialService.selectTbMaterialList(tbMaterial);
        ExcelUtil<TbMaterial> util = new ExcelUtil<TbMaterial>(TbMaterial.class);
        return util.exportExcel(list, "material");
    }

    /**
     * 获取衣服原料
详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:material:query')")
    @GetMapping(value = "/{materialId}")
    public AjaxResult getInfo(@PathVariable("materialId") Long materialId)
    {
        return AjaxResult.success(tbMaterialService.selectTbMaterialById(materialId));
    }

    /**
     * 新增衣服原料

     */
    @PreAuthorize("@ss.hasPermi('goods:material:add')")
    @Log(title = "衣服原料 ", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbMaterial tbMaterial)
    {
        return toAjax(tbMaterialService.insertTbMaterial(tbMaterial));
    }

    /**
     * 修改衣服原料

     */
    @PreAuthorize("@ss.hasPermi('goods:material:edit')")
    @Log(title = "衣服原料 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbMaterial tbMaterial)
    {
        return toAjax(tbMaterialService.updateTbMaterial(tbMaterial));
    }

    /**
     * 删除衣服原料

     */
    @PreAuthorize("@ss.hasPermi('goods:material:remove')")
    @Log(title = "衣服原料 ", businessType = BusinessType.DELETE)
	@DeleteMapping("/{materialIds}")
    public AjaxResult remove(@PathVariable Long[] materialIds)
    {
        return toAjax(tbMaterialService.deleteTbMaterialByIds(materialIds));
    }
}
