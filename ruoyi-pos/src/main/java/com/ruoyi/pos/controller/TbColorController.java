package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbColor;
import com.ruoyi.pos.service.ITbColorService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服颜色Controller
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
@RestController
@RequestMapping("/goods/color")
public class TbColorController extends BaseController
{
    @Autowired
    private ITbColorService tbColorService;

    /**
     * 查询衣服颜色列表
     */
 //   @PreAuthorize("@ss.hasPermi('goods:color:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbColor tbColor)
    {
        startPage();
        List<TbColor> list = tbColorService.selectTbColorList(tbColor);
        return getDataTable(list);
    }

    /**
     * 导出衣服颜色列表
     */
    @PreAuthorize("@ss.hasPermi('goods:color:export')")
    @Log(title = "衣服颜色", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbColor tbColor)
    {
        List<TbColor> list = tbColorService.selectTbColorList(tbColor);
        ExcelUtil<TbColor> util = new ExcelUtil<TbColor>(TbColor.class);
        return util.exportExcel(list, "color");
    }

    /**
     * 获取衣服颜色详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:color:query')")
    @GetMapping(value = "/{colorId}")
    public AjaxResult getInfo(@PathVariable("colorId") Long colorId)
    {
        return AjaxResult.success(tbColorService.selectTbColorById(colorId));
    }

    /**
     * 新增衣服颜色
     */
    @PreAuthorize("@ss.hasPermi('goods:color:add')")
    @Log(title = "衣服颜色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbColor tbColor)
    {
        return toAjax(tbColorService.insertTbColor(tbColor));
    }

    /**
     * 修改衣服颜色
     */
    @PreAuthorize("@ss.hasPermi('goods:color:edit')")
    @Log(title = "衣服颜色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbColor tbColor)
    {
        return toAjax(tbColorService.updateTbColor(tbColor));
    }

    /**
     * 删除衣服颜色
     */
    @PreAuthorize("@ss.hasPermi('goods:color:remove')")
    @Log(title = "衣服颜色", businessType = BusinessType.DELETE)
	@DeleteMapping("/{colorIds}")
    public AjaxResult remove(@PathVariable Long[] colorIds)
    {
        return toAjax(tbColorService.deleteTbColorByIds(colorIds));
    }
}
