package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbOrderid;
import com.ruoyi.pos.service.ITbOrderidService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单顾客
Controller
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@RestController
@RequestMapping("/orders/customer")
public class TbOrderidController extends BaseController
{
    @Autowired
    private ITbOrderidService tbOrderidService;

    /**
     * 查询订单顾客
列表
     */
   // @PreAuthorize("@ss.hasPermi('orders:customer:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbOrderid tbOrderid)
    {
        startPage();
        List<TbOrderid> list = tbOrderidService.selectTbOrderidList(tbOrderid);
        return getDataTable(list);
    }

    /**
     * 导出订单顾客
列表
     */
    @PreAuthorize("@ss.hasPermi('orders:customer:export')")
    @Log(title = "订单顾客 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbOrderid tbOrderid)
    {
        List<TbOrderid> list = tbOrderidService.selectTbOrderidList(tbOrderid);
        ExcelUtil<TbOrderid> util = new ExcelUtil<TbOrderid>(TbOrderid.class);
        return util.exportExcel(list, "customer");
    }

    /**
     * 获取订单顾客
详细信息
     */
    @PreAuthorize("@ss.hasPermi('orders:customer:query')")
    @GetMapping(value = "/{ordId}")
    public AjaxResult getInfo(@PathVariable("ordId") Long ordId)
    {
        return AjaxResult.success(tbOrderidService.selectTbOrderidById(ordId));
    }

    /**
     * 新增订单顾客

     */
    @PreAuthorize("@ss.hasPermi('orders:customer:add')")
    @Log(title = "订单顾客 ", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbOrderid tbOrderid)
    {
        return toAjax(tbOrderidService.insertTbOrderid(tbOrderid));
    }

    /**
     * 修改订单顾客

     */
    @PreAuthorize("@ss.hasPermi('orders:customer:edit')")
    @Log(title = "订单顾客 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbOrderid tbOrderid)
    {
        return toAjax(tbOrderidService.updateTbOrderid(tbOrderid));
    }

    /**
     * 删除订单顾客

     */
    @PreAuthorize("@ss.hasPermi('orders:customer:remove')")
    @Log(title = "订单顾客 ", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ordIds}")
    public AjaxResult remove(@PathVariable Long[] ordIds)
    {
        return toAjax(tbOrderidService.deleteTbOrderidByIds(ordIds));
    }
}
