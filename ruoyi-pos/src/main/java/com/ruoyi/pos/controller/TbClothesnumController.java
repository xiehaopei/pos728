package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbClothesnum;
import com.ruoyi.pos.service.ITbClothesnumService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服库存





Controller
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/stores/clothesnum")
public class TbClothesnumController extends BaseController
{
    @Autowired
    private ITbClothesnumService tbClothesnumService;

    /**
     * 查询衣服库存





列表
     */
  //  @PreAuthorize("@ss.hasPermi('stores:clothesnum:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbClothesnum tbClothesnum)
    {
        startPage();
        List<TbClothesnum> list = tbClothesnumService.selectTbClothesnumList(tbClothesnum);
        return getDataTable(list);
    }

    /**
     * 导出衣服库存





列表
     */
   @PreAuthorize("@ss.hasPermi('stores:clothesnum:export')")
    @Log(title = "衣服库存", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbClothesnum tbClothesnum)
    {
        List<TbClothesnum> list = tbClothesnumService.selectTbClothesnumList(tbClothesnum);
        ExcelUtil<TbClothesnum> util = new ExcelUtil<TbClothesnum>(TbClothesnum.class);
        return util.exportExcel(list, "clothesnum");
    }

    /**
     * 获取衣服库存





详细信息
     */
    @PreAuthorize("@ss.hasPermi('stores:clothesnum:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tbClothesnumService.selectTbClothesnumById(id));
    }

    /**
     * 新增衣服库存






     */
    @PreAuthorize("@ss.hasPermi('stores:clothesnum:add')")
    @Log(title = "衣服库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbClothesnum tbClothesnum)
    {
        return toAjax(tbClothesnumService.insertTbClothesnum(tbClothesnum));
    }

    /**
     * 修改衣服库存






     */
    @PreAuthorize("@ss.hasPermi('stores:clothesnum:edit')")
    @Log(title = "衣服库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbClothesnum tbClothesnum)
    {
        return toAjax(tbClothesnumService.updateTbClothesnum(tbClothesnum));
    }

    /**
     * 删除衣服库存






     */
    @PreAuthorize("@ss.hasPermi('stores:clothesnum:remove')")
    @Log(title = "衣服库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tbClothesnumService.deleteTbClothesnumByIds(ids));
    }
}
