package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbOrder;
import com.ruoyi.pos.service.ITbOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单信息Controller
 * 
 * @author ruoyi
 * @date 2021-06-14
 */
@RestController
@RequestMapping("/orders/order")
public class TbOrderController extends BaseController
{
    @Autowired
    private ITbOrderService tbOrderService;

    /**
     * 查询订单信息列表
     */
 //   @PreAuthorize("@ss.hasPermi('orders:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbOrder tbOrder)
    {
        startPage();
        List<TbOrder> list = tbOrderService.selectTbOrderList(tbOrder);
        return getDataTable(list);
    }

    /**
     * 导出订单信息列表
     */
    @PreAuthorize("@ss.hasPermi('orders:order:export')")
    @Log(title = "订单信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbOrder tbOrder)
    {
        List<TbOrder> list = tbOrderService.selectTbOrderList(tbOrder);
        ExcelUtil<TbOrder> util = new ExcelUtil<TbOrder>(TbOrder.class);
        return util.exportExcel(list, "order");
    }

    /**
     * 获取订单信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('orders:order:query')")
    @GetMapping(value = "/{ordId}")
    public AjaxResult getInfo(@PathVariable("ordId") Long ordId)
    {
        return AjaxResult.success(tbOrderService.selectTbOrderById(ordId));
    }

    /**
     * 新增订单信息
     */
    @PreAuthorize("@ss.hasPermi('orders:order:add')")
    @Log(title = "订单信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbOrder tbOrder)
    {
        return toAjax(tbOrderService.insertTbOrder(tbOrder));
    }

    /**
     * 修改订单信息
     */
    @PreAuthorize("@ss.hasPermi('orders:order:edit')")
    @Log(title = "订单信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbOrder tbOrder)
    {
        return toAjax(tbOrderService.updateTbOrder(tbOrder));
    }

    /**
     * 删除订单信息
     */
    @PreAuthorize("@ss.hasPermi('orders:order:remove')")
    @Log(title = "订单信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ordIds}")
    public AjaxResult remove(@PathVariable Long[] ordIds)
    {
        return toAjax(tbOrderService.deleteTbOrderByIds(ordIds));
    }
}
