package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbPayment;
import com.ruoyi.pos.service.ITbPaymentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单支付



Controller
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@RestController
@RequestMapping("/orders/payment")
public class TbPaymentController extends BaseController
{
    @Autowired
    private ITbPaymentService tbPaymentService;

    /**
     * 查询订单支付



列表
     */
   // @PreAuthorize("@ss.hasPermi('orders:payment:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbPayment tbPayment)
    {
        startPage();
        List<TbPayment> list = tbPaymentService.selectTbPaymentList(tbPayment);
        return getDataTable(list);
    }

    /**
     * 导出订单支付



列表
     */
    @PreAuthorize("@ss.hasPermi('orders:payment:export')")
    @Log(title = "订单支付 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbPayment tbPayment)
    {
        List<TbPayment> list = tbPaymentService.selectTbPaymentList(tbPayment);
        ExcelUtil<TbPayment> util = new ExcelUtil<TbPayment>(TbPayment.class);
        return util.exportExcel(list, "payment");
    }

    /**
     * 获取订单支付



详细信息
     */
    @PreAuthorize("@ss.hasPermi('orders:payment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tbPaymentService.selectTbPaymentById(id));
    }

    /**
     * 新增订单支付




     */
    @PreAuthorize("@ss.hasPermi('orders:payment:add')")
    @Log(title = "订单支付 ", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbPayment tbPayment)
    {
        return toAjax(tbPaymentService.insertTbPayment(tbPayment));
    }

    /**
     * 修改订单支付




     */
    @PreAuthorize("@ss.hasPermi('orders:payment:edit')")
    @Log(title = "订单支付 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbPayment tbPayment)
    {
        return toAjax(tbPaymentService.updateTbPayment(tbPayment));
    }

    /**
     * 删除订单支付




     */
    @PreAuthorize("@ss.hasPermi('orders:payment:remove')")
    @Log(title = "订单支付", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tbPaymentService.deleteTbPaymentByIds(ids));
    }
}
