package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbSize;
import com.ruoyi.pos.service.ITbSizeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 尺寸Controller
 * 
 * @author ruoyi
<<<<<<< HEAD
 * @date 2021-05-31
=======
 * @date 2021-06-01
>>>>>>> service
 */
@RestController
@RequestMapping("/goods/size")
public class TbSizeController extends BaseController
{
    @Autowired
    private ITbSizeService tbSizeService;

    /**
     * 查询尺寸列表
     */
    @PreAuthorize("@ss.hasPermi('goods:size:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbSize tbSize)
    {
        startPage();
        List<TbSize> list = tbSizeService.selectTbSizeList(tbSize);
        return getDataTable(list);
    }

    /**
     * 导出尺寸列表
     */
    @PreAuthorize("@ss.hasPermi('goods:size:export')")
    @GetMapping("/export")
    public AjaxResult export(TbSize tbSize)
    {
        List<TbSize> list = tbSizeService.selectTbSizeList(tbSize);
        ExcelUtil<TbSize> util = new ExcelUtil<TbSize>(TbSize.class);
        return util.exportExcel(list, "size");
    }

    /**
     * 获取尺寸详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:size:query')")
    @GetMapping(value = "/{sizeId}")
    public AjaxResult getInfo(@PathVariable("sizeId") Long sizeId)
    {
        return AjaxResult.success(tbSizeService.selectTbSizeById(sizeId));
    }

    /**
     * 新增尺寸
     */
    @PreAuthorize("@ss.hasPermi('goods:size:add')")
    @Log(title = "尺寸", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbSize tbSize)
    {
        return toAjax(tbSizeService.insertTbSize(tbSize));
    }

    /**
     * 修改尺寸
     */
    @PreAuthorize("@ss.hasPermi('goods:size:edit')")
    @PutMapping
    public AjaxResult edit(@RequestBody TbSize tbSize)
    {
        return toAjax(tbSizeService.updateTbSize(tbSize));
    }

    /**
     * 删除尺寸
     */
    @PreAuthorize("@ss.hasPermi('goods:size:remove')")
	@DeleteMapping("/{sizeIds}")
    public AjaxResult remove(@PathVariable Long[] sizeIds)
    {
        return toAjax(tbSizeService.deleteTbSizeByIds(sizeIds));
    }
}
