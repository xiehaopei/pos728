package com.ruoyi.pos.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pos.domain.TbClassify;
import com.ruoyi.pos.service.ITbClassifyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 衣服分类Controller
 * 
 * @author zsw
 * @date 2021-05-22
 */
@RestController
@RequestMapping("/goods/classify")
public class TbClassifyController extends BaseController
{
    @Autowired
    private ITbClassifyService tbClassifyService;

    /**
     * 查询衣服分类列表
     */
 //   @PreAuthorize("@ss.hasPermi('goods:classify:list')")
    @GetMapping("/list")
    public TableDataInfo list(TbClassify tbClassify)
    {
        startPage();
        List<TbClassify> list = tbClassifyService.selectTbClassifyList(tbClassify);
        return getDataTable(list);
    }

    /**
     * 导出衣服分类列表
     */
    @PreAuthorize("@ss.hasPermi('goods:classify:export')")
    @Log(title = "衣服分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TbClassify tbClassify)
    {
        List<TbClassify> list = tbClassifyService.selectTbClassifyList(tbClassify);
        ExcelUtil<TbClassify> util = new ExcelUtil<TbClassify>(TbClassify.class);
        return util.exportExcel(list, "classify");
    }

    /**
     * 获取衣服分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('goods:classify:query')")
    @GetMapping(value = "/{classifyId}")
    public AjaxResult getInfo(@PathVariable("classifyId") Long classifyId)
    {
        return AjaxResult.success(tbClassifyService.selectTbClassifyById(classifyId));
    }

    /**
     * 新增衣服分类
     */
    @PreAuthorize("@ss.hasPermi('goods:classify:add')")
    @Log(title = "衣服分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TbClassify tbClassify)
    {
        return toAjax(tbClassifyService.insertTbClassify(tbClassify));
    }

    /**
     * 修改衣服分类
     */
    @PreAuthorize("@ss.hasPermi('goods:classify:edit')")
    @Log(title = "衣服分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TbClassify tbClassify)
    {
        return toAjax(tbClassifyService.updateTbClassify(tbClassify));
    }

    /**
     * 删除衣服分类
     */
    @PreAuthorize("@ss.hasPermi('goods:classify:remove')")
    @Log(title = "衣服分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{classifyIds}")
    public AjaxResult remove(@PathVariable Long[] classifyIds)
    {
        return toAjax(tbClassifyService.deleteTbClassifyByIds(classifyIds));
    }
}
