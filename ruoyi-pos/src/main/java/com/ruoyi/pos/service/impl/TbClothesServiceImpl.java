package com.ruoyi.pos.service.impl;

import java.util.List;

import com.ruoyi.pos.mapper.TbColorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbClothesMapper;
import com.ruoyi.pos.domain.TbClothes;
import com.ruoyi.pos.service.ITbClothesService;

/**
 * 衣服商品






Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-04
 */
@Service
public class TbClothesServiceImpl implements ITbClothesService 
{
    @Autowired
    private TbClothesMapper tbClothesMapper;

    /**
     * 查询衣服商品







     * 
     * @param id 衣服商品






ID
     * @return 衣服商品







     */
    @Override
    public TbClothes selectTbClothesById(Long id)
    {
        return tbClothesMapper.selectTbClothesById(id);
    }

    /**
     * 查询衣服商品






列表
     * 
     * @param tbClothes 衣服商品







     * @return 衣服商品







     */
    @Override
    public List<TbClothes> selectTbClothesList(TbClothes tbClothes)
    {
        return tbClothesMapper.selectTbClothesList(tbClothes);
    }

    /**
     * 新增衣服商品







     * 
     * @param tbClothes 衣服商品







     * @return 结果
     */
    @Override
    public int insertTbClothes(TbClothes tbClothes)
    {
        return tbClothesMapper.insertTbClothes(tbClothes);
    }

    /**
     * 修改衣服商品







     * 
     * @param tbClothes 衣服商品







     * @return 结果
     */
    @Override
    public int updateTbClothes(TbClothes tbClothes)
    {
        return tbClothesMapper.updateTbClothes(tbClothes);
    }

    /**
     * 批量删除衣服商品







     * 
     * @param ids 需要删除的衣服商品






ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesByIds(Long[] ids)
    {
        return tbClothesMapper.deleteTbClothesByIds(ids);
    }

    /**
     * 删除衣服商品






信息
     * 
     * @param id 衣服商品






ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesById(Long id)
    {
        return tbClothesMapper.deleteTbClothesById(id);
    }


}
