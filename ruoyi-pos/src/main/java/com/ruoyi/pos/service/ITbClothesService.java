package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbClothes;

/**
 * 衣服商品






Service接口
 * 
 * @author ruoyi
 * @date 2021-06-04
 */
public interface ITbClothesService 
{
    /**
     * 查询衣服商品







     * 
     * @param id 衣服商品






ID
     * @return 衣服商品







     */
    public TbClothes selectTbClothesById(Long id);

    /**
     * 查询衣服商品






列表
     * 
     * @param tbClothes 衣服商品







     * @return 衣服商品






集合
     */
    public List<TbClothes> selectTbClothesList(TbClothes tbClothes);

    /**
     * 新增衣服商品







     * 
     * @param tbClothes 衣服商品







     * @return 结果
     */
    public int insertTbClothes(TbClothes tbClothes);

    /**
     * 修改衣服商品







     * 
     * @param tbClothes 衣服商品







     * @return 结果
     */
    public int updateTbClothes(TbClothes tbClothes);

    /**
     * 批量删除衣服商品







     * 
     * @param ids 需要删除的衣服商品






ID
     * @return 结果
     */
    public int deleteTbClothesByIds(Long[] ids);

    /**
     * 删除衣服商品






信息
     * 
     * @param id 衣服商品






ID
     * @return 结果
     */
    public int deleteTbClothesById(Long id);
}
