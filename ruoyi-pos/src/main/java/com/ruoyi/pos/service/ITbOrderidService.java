package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbOrderid;

/**
 * 订单顾客
Service接口
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public interface ITbOrderidService 
{
    /**
     * 查询订单顾客

     * 
     * @param ordId 订单顾客
ID
     * @return 订单顾客

     */
    public TbOrderid selectTbOrderidById(Long ordId);

    /**
     * 查询订单顾客
列表
     * 
     * @param tbOrderid 订单顾客

     * @return 订单顾客
集合
     */
    public List<TbOrderid> selectTbOrderidList(TbOrderid tbOrderid);

    /**
     * 新增订单顾客

     * 
     * @param tbOrderid 订单顾客

     * @return 结果
     */
    public int insertTbOrderid(TbOrderid tbOrderid);

    /**
     * 修改订单顾客

     * 
     * @param tbOrderid 订单顾客

     * @return 结果
     */
    public int updateTbOrderid(TbOrderid tbOrderid);

    /**
     * 批量删除订单顾客

     * 
     * @param ordIds 需要删除的订单顾客
ID
     * @return 结果
     */
    public int deleteTbOrderidByIds(Long[] ordIds);

    /**
     * 删除订单顾客
信息
     * 
     * @param ordId 订单顾客
ID
     * @return 结果
     */
    public int deleteTbOrderidById(Long ordId);
}
