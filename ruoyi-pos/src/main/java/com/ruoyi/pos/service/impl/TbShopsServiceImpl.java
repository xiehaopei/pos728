package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbShopsMapper;
import com.ruoyi.pos.domain.TbShops;
import com.ruoyi.pos.service.ITbShopsService;

/**
 * 商铺信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@Service
public class TbShopsServiceImpl implements ITbShopsService 
{
    @Autowired
    private TbShopsMapper tbShopsMapper;

    /**
     * 查询商铺信息
     * 
     * @param shopId 商铺信息ID
     * @return 商铺信息
     */
    @Override
    public TbShops selectTbShopsById(String shopId)
    {
        return tbShopsMapper.selectTbShopsById(shopId);
    }

    /**
     * 查询商铺信息列表
     * 
     * @param tbShops 商铺信息
     * @return 商铺信息
     */
    @Override
    public List<TbShops> selectTbShopsList(TbShops tbShops)
    {
        return tbShopsMapper.selectTbShopsList(tbShops);
    }

    /**
     * 新增商铺信息
     * 
     * @param tbShops 商铺信息
     * @return 结果
     */
    @Override
    public int insertTbShops(TbShops tbShops)
    {
        return tbShopsMapper.insertTbShops(tbShops);
    }

    /**
     * 修改商铺信息
     * 
     * @param tbShops 商铺信息
     * @return 结果
     */
    @Override
    public int updateTbShops(TbShops tbShops)
    {
        return tbShopsMapper.updateTbShops(tbShops);
    }

    /**
     * 批量删除商铺信息
     * 
     * @param shopIds 需要删除的商铺信息ID
     * @return 结果
     */
    @Override
    public int deleteTbShopsByIds(String[] shopIds)
    {
        return tbShopsMapper.deleteTbShopsByIds(shopIds);
    }

    /**
     * 删除商铺信息信息
     * 
     * @param shopId 商铺信息ID
     * @return 结果
     */
    @Override
    public int deleteTbShopsById(String shopId)
    {
        return tbShopsMapper.deleteTbShopsById(shopId);
    }
}
