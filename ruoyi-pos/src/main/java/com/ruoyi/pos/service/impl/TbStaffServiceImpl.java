package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbStaffMapper;
import com.ruoyi.pos.domain.TbStaff;
import com.ruoyi.pos.service.ITbStaffService;

/**
 * 服装店工作人员Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@Service
public class TbStaffServiceImpl implements ITbStaffService 
{
    @Autowired
    private TbStaffMapper tbStaffMapper;

    /**
     * 查询服装店工作人员
     * 
     * @param sId 服装店工作人员ID
     * @return 服装店工作人员
     */
    @Override
    public TbStaff selectTbStaffById(String sId)
    {
        return tbStaffMapper.selectTbStaffById(sId);
    }

    /**
     * 查询服装店工作人员列表
     * 
     * @param tbStaff 服装店工作人员
     * @return 服装店工作人员
     */
    @Override
    public List<TbStaff> selectTbStaffList(TbStaff tbStaff)
    {
        return tbStaffMapper.selectTbStaffList(tbStaff);
    }

    /**
     * 新增服装店工作人员
     * 
     * @param tbStaff 服装店工作人员
     * @return 结果
     */
    @Override
    public int insertTbStaff(TbStaff tbStaff)
    {
        return tbStaffMapper.insertTbStaff(tbStaff);
    }

    /**
     * 修改服装店工作人员
     * 
     * @param tbStaff 服装店工作人员
     * @return 结果
     */
    @Override
    public int updateTbStaff(TbStaff tbStaff)
    {
        return tbStaffMapper.updateTbStaff(tbStaff);
    }

    /**
     * 批量删除服装店工作人员
     * 
     * @param sIds 需要删除的服装店工作人员ID
     * @return 结果
     */
    @Override
    public int deleteTbStaffByIds(String[] sIds)
    {
        return tbStaffMapper.deleteTbStaffByIds(sIds);
    }

    /**
     * 删除服装店工作人员信息
     * 
     * @param sId 服装店工作人员ID
     * @return 结果
     */
    @Override
    public int deleteTbStaffById(String sId)
    {
        return tbStaffMapper.deleteTbStaffById(sId);
    }
}
