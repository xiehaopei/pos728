package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbReceivablesMapper;
import com.ruoyi.pos.domain.TbReceivables;
import com.ruoyi.pos.service.ITbReceivablesService;

/**
 * 收款状况



Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@Service
public class TbReceivablesServiceImpl implements ITbReceivablesService 
{
    @Autowired
    private TbReceivablesMapper tbReceivablesMapper;

    /**
     * 查询收款状况




     * 
     * @param id 收款状况



ID
     * @return 收款状况




     */
    @Override
    public TbReceivables selectTbReceivablesById(Long id)
    {
        return tbReceivablesMapper.selectTbReceivablesById(id);
    }

    /**
     * 查询收款状况



列表
     * 
     * @param tbReceivables 收款状况




     * @return 收款状况




     */
    @Override
    public List<TbReceivables> selectTbReceivablesList(TbReceivables tbReceivables)
    {
        return tbReceivablesMapper.selectTbReceivablesList(tbReceivables);
    }

    /**
     * 新增收款状况




     * 
     * @param tbReceivables 收款状况




     * @return 结果
     */
    @Override
    public int insertTbReceivables(TbReceivables tbReceivables)
    {
        return tbReceivablesMapper.insertTbReceivables(tbReceivables);
    }

    /**
     * 修改收款状况




     * 
     * @param tbReceivables 收款状况




     * @return 结果
     */
    @Override
    public int updateTbReceivables(TbReceivables tbReceivables)
    {
        return tbReceivablesMapper.updateTbReceivables(tbReceivables);
    }

    /**
     * 批量删除收款状况




     * 
     * @param ids 需要删除的收款状况



ID
     * @return 结果
     */
    @Override
    public int deleteTbReceivablesByIds(Long[] ids)
    {
        return tbReceivablesMapper.deleteTbReceivablesByIds(ids);
    }

    /**
     * 删除收款状况



信息
     * 
     * @param id 收款状况



ID
     * @return 结果
     */
    @Override
    public int deleteTbReceivablesById(Long id)
    {
        return tbReceivablesMapper.deleteTbReceivablesById(id);
    }
}
