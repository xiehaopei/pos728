package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbClothesnumMapper;
import com.ruoyi.pos.domain.TbClothesnum;
import com.ruoyi.pos.service.ITbClothesnumService;

/**
 * 衣服库存





Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
@Service
public class TbClothesnumServiceImpl implements ITbClothesnumService 
{
    @Autowired
    private TbClothesnumMapper tbClothesnumMapper;

    /**
     * 查询衣服库存






     * 
     * @param id 衣服库存





ID
     * @return 衣服库存






     */
    @Override
    public TbClothesnum selectTbClothesnumById(Long id)
    {
        return tbClothesnumMapper.selectTbClothesnumById(id);
    }

    @Override
    public TbClothesnum selectTbClothesnumByClothId(Long id) {
        return tbClothesnumMapper.selectTbClothesnumByClothId(id);
    }

    /**
     * 查询衣服库存





列表
     * 
     * @param tbClothesnum 衣服库存






     * @return 衣服库存






     */
    @Override
    public List<TbClothesnum> selectTbClothesnumList(TbClothesnum tbClothesnum)
    {
        return tbClothesnumMapper.selectTbClothesnumList(tbClothesnum);
    }

    /**
     * 新增衣服库存






     * 
     * @param tbClothesnum 衣服库存






     * @return 结果
     */
    @Override
    public int insertTbClothesnum(TbClothesnum tbClothesnum)
    {
        return tbClothesnumMapper.insertTbClothesnum(tbClothesnum);
    }

    /**
     * 修改衣服库存






     * 
     * @param tbClothesnum 衣服库存






     * @return 结果
     */
    @Override
    public int updateTbClothesnum(TbClothesnum tbClothesnum)
    {
        return tbClothesnumMapper.updateTbClothesnum(tbClothesnum);
    }

    /**
     * 批量删除衣服库存






     * 
     * @param ids 需要删除的衣服库存





ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesnumByIds(Long[] ids)
    {
        return tbClothesnumMapper.deleteTbClothesnumByIds(ids);
    }

    /**
     * 删除衣服库存





信息
     * 
     * @param id 衣服库存





ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesnumById(Long id)
    {
        return tbClothesnumMapper.deleteTbClothesnumById(id);
    }
}
