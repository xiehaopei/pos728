package com.ruoyi.pos.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.pos.domain.TbClothes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbOrderMapper;
import com.ruoyi.pos.domain.TbOrder;
import com.ruoyi.pos.service.ITbOrderService;

/**
 * 订单信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-14
 */
@Service
public class TbOrderServiceImpl implements ITbOrderService 
{
    @Autowired
    private TbOrderMapper tbOrderMapper;

    /**
     * 查询订单信息
     * 
     * @param ordId 订单信息ID
     * @return 订单信息
     */
    @Override
    public TbOrder selectTbOrderById(Long ordId)
    {
        return tbOrderMapper.selectTbOrderById(ordId);
    }

    /**
     * 查询订单信息列表
     * 
     * @param tbOrder 订单信息
     * @return 订单信息
     */
    @Override
    public List<TbOrder> selectTbOrderList(TbOrder tbOrder)
    {
        return tbOrderMapper.selectTbOrderList(tbOrder);
    }

    /**
     * 新增订单信息
     * 
     * @param tbOrder 订单信息
     * @return 结果
     */
    @Override
    public int insertTbOrder(TbOrder tbOrder)
    {
        String json = tbOrder.getClothList().replace("[", "").replace("]", "");
        System.out.println(json );
        List<TbClothes> tbClothes = new ArrayList<>();
        List<TbClothes> list = JSONObject.parseArray(JSONObject.toJSONString(json), TbClothes.class);
        System.out.println(list);
        return tbOrderMapper.insertTbOrder(tbOrder);
    }

    /**
     * 修改订单信息
     * 
     * @param tbOrder 订单信息
     * @return 结果
     */
    @Override
    public int updateTbOrder(TbOrder tbOrder)
    {
        return tbOrderMapper.updateTbOrder(tbOrder);
    }

    /**
     * 批量删除订单信息
     * 
     * @param ordIds 需要删除的订单信息ID
     * @return 结果
     */
    @Override
    public int deleteTbOrderByIds(Long[] ordIds)
    {
        return tbOrderMapper.deleteTbOrderByIds(ordIds);
    }

    /**
     * 删除订单信息信息
     * 
     * @param ordId 订单信息ID
     * @return 结果
     */
    @Override
    public int deleteTbOrderById(Long ordId)
    {
        return tbOrderMapper.deleteTbOrderById(ordId);
    }
}
