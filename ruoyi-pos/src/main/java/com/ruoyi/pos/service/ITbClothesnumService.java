package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbClothesnum;

/**
 * 衣服库存





Service接口
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
public interface ITbClothesnumService 
{
    /**
     * 查询衣服库存






     * 
     * @param id 衣服库存





ID
     * @return 衣服库存






     */
    public TbClothesnum selectTbClothesnumById(Long id);
    public TbClothesnum selectTbClothesnumByClothId(Long id);
    /**
     * 查询衣服库存





列表
     * 
     * @param tbClothesnum 衣服库存






     * @return 衣服库存





集合
     */
    public List<TbClothesnum> selectTbClothesnumList(TbClothesnum tbClothesnum);

    /**
     * 新增衣服库存






     * 
     * @param tbClothesnum 衣服库存






     * @return 结果
     */
    public int insertTbClothesnum(TbClothesnum tbClothesnum);

    /**
     * 修改衣服库存






     * 
     * @param tbClothesnum 衣服库存






     * @return 结果
     */
    public int updateTbClothesnum(TbClothesnum tbClothesnum);

    /**
     * 批量删除衣服库存






     * 
     * @param ids 需要删除的衣服库存





ID
     * @return 结果
     */
    public int deleteTbClothesnumByIds(Long[] ids);

    /**
     * 删除衣服库存





信息
     * 
     * @param id 衣服库存





ID
     * @return 结果
     */
    public int deleteTbClothesnumById(Long id);
}
