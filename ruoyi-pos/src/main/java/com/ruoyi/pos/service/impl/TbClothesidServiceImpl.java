package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbClothesidMapper;
import com.ruoyi.pos.domain.TbClothesid;
import com.ruoyi.pos.service.ITbClothesidService;

/**
 * 衣服关系
Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
@Service
public class TbClothesidServiceImpl implements ITbClothesidService 
{
    @Autowired
    private TbClothesidMapper tbClothesidMapper;

    /**
     * 查询衣服关系

     * 
     * @param clothId 衣服关系
ID
     * @return 衣服关系

     */
    @Override
    public TbClothesid selectTbClothesidById(Long clothId)
    {
        return tbClothesidMapper.selectTbClothesidById(clothId);
    }

    /**
     * 查询衣服关系
列表
     * 
     * @param tbClothesid 衣服关系

     * @return 衣服关系

     */
    @Override
    public List<TbClothesid> selectTbClothesidList(TbClothesid tbClothesid)
    {
        return tbClothesidMapper.selectTbClothesidList(tbClothesid);
    }

    /**
     * 新增衣服关系

     * 
     * @param tbClothesid 衣服关系

     * @return 结果
     */
    @Override
    public int insertTbClothesid(TbClothesid tbClothesid)
    {
        return tbClothesidMapper.insertTbClothesid(tbClothesid);
    }

    /**
     * 修改衣服关系

     * 
     * @param tbClothesid 衣服关系

     * @return 结果
     */
    @Override
    public int updateTbClothesid(TbClothesid tbClothesid)
    {
        return tbClothesidMapper.updateTbClothesid(tbClothesid);
    }

    /**
     * 批量删除衣服关系

     * 
     * @param clothIds 需要删除的衣服关系
ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesidByIds(Long[] clothIds)
    {
        return tbClothesidMapper.deleteTbClothesidByIds(clothIds);
    }

    /**
     * 删除衣服关系
信息
     * 
     * @param clothId 衣服关系
ID
     * @return 结果
     */
    @Override
    public int deleteTbClothesidById(Long clothId)
    {
        return tbClothesidMapper.deleteTbClothesidById(clothId);
    }
}
