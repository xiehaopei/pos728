package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbWarehouse;

/**
 * 衣服仓库Service接口
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public interface ITbWarehouseService 
{
    /**
     * 查询衣服仓库
     * 
     * @param warehouseId 衣服仓库ID
     * @return 衣服仓库
     */
    public TbWarehouse selectTbWarehouseById(String warehouseId);

    /**
     * 查询衣服仓库列表
     * 
     * @param tbWarehouse 衣服仓库
     * @return 衣服仓库集合
     */
    public List<TbWarehouse> selectTbWarehouseList(TbWarehouse tbWarehouse);

    /**
     * 新增衣服仓库
     * 
     * @param tbWarehouse 衣服仓库
     * @return 结果
     */
    public int insertTbWarehouse(TbWarehouse tbWarehouse);

    /**
     * 修改衣服仓库
     * 
     * @param tbWarehouse 衣服仓库
     * @return 结果
     */
    public int updateTbWarehouse(TbWarehouse tbWarehouse);

    /**
     * 批量删除衣服仓库
     * 
     * @param warehouseIds 需要删除的衣服仓库ID
     * @return 结果
     */
    public int deleteTbWarehouseByIds(String[] warehouseIds);

    /**
     * 删除衣服仓库信息
     * 
     * @param warehouseId 衣服仓库ID
     * @return 结果
     */
    public int deleteTbWarehouseById(String warehouseId);
}
