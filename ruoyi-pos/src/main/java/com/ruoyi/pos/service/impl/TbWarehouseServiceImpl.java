package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbWarehouseMapper;
import com.ruoyi.pos.domain.TbWarehouse;
import com.ruoyi.pos.service.ITbWarehouseService;

/**
 * 衣服仓库Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
@Service
public class TbWarehouseServiceImpl implements ITbWarehouseService 
{
    @Autowired
    private TbWarehouseMapper tbWarehouseMapper;

    /**
     * 查询衣服仓库
     * 
     * @param warehouseId 衣服仓库ID
     * @return 衣服仓库
     */
    @Override
    public TbWarehouse selectTbWarehouseById(String warehouseId)
    {
        return tbWarehouseMapper.selectTbWarehouseById(warehouseId);
    }

    /**
     * 查询衣服仓库列表
     * 
     * @param tbWarehouse 衣服仓库
     * @return 衣服仓库
     */
    @Override
    public List<TbWarehouse> selectTbWarehouseList(TbWarehouse tbWarehouse)
    {
        return tbWarehouseMapper.selectTbWarehouseList(tbWarehouse);
    }

    /**
     * 新增衣服仓库
     * 
     * @param tbWarehouse 衣服仓库
     * @return 结果
     */
    @Override
    public int insertTbWarehouse(TbWarehouse tbWarehouse)
    {
        return tbWarehouseMapper.insertTbWarehouse(tbWarehouse);
    }

    /**
     * 修改衣服仓库
     * 
     * @param tbWarehouse 衣服仓库
     * @return 结果
     */
    @Override
    public int updateTbWarehouse(TbWarehouse tbWarehouse)
    {
        return tbWarehouseMapper.updateTbWarehouse(tbWarehouse);
    }

    /**
     * 批量删除衣服仓库
     * 
     * @param warehouseIds 需要删除的衣服仓库ID
     * @return 结果
     */
    @Override
    public int deleteTbWarehouseByIds(String[] warehouseIds)
    {
        return tbWarehouseMapper.deleteTbWarehouseByIds(warehouseIds);
    }

    /**
     * 删除衣服仓库信息
     * 
     * @param warehouseId 衣服仓库ID
     * @return 结果
     */
    @Override
    public int deleteTbWarehouseById(String warehouseId)
    {
        return tbWarehouseMapper.deleteTbWarehouseById(warehouseId);
    }
}
