package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbPayment;

/**
 * 订单支付



Service接口
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public interface ITbPaymentService 
{
    /**
     * 查询订单支付




     * 
     * @param id 订单支付



ID
     * @return 订单支付




     */
    public TbPayment selectTbPaymentById(Long id);

    /**
     * 查询订单支付



列表
     * 
     * @param tbPayment 订单支付




     * @return 订单支付



集合
     */
    public List<TbPayment> selectTbPaymentList(TbPayment tbPayment);

    /**
     * 新增订单支付




     * 
     * @param tbPayment 订单支付




     * @return 结果
     */
    public int insertTbPayment(TbPayment tbPayment);

    /**
     * 修改订单支付




     * 
     * @param tbPayment 订单支付




     * @return 结果
     */
    public int updateTbPayment(TbPayment tbPayment);

    /**
     * 批量删除订单支付




     * 
     * @param ids 需要删除的订单支付



ID
     * @return 结果
     */
    public int deleteTbPaymentByIds(Long[] ids);

    /**
     * 删除订单支付



信息
     * 
     * @param id 订单支付



ID
     * @return 结果
     */
    public int deleteTbPaymentById(Long id);
}
