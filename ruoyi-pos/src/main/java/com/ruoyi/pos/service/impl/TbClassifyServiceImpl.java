package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbClassifyMapper;
import com.ruoyi.pos.domain.TbClassify;
import com.ruoyi.pos.service.ITbClassifyService;

/**
 * 分类id
分类名称Service业务层处理
 * 
 * @author zsw
 * @date 2021-05-26
 */
@Service
public class TbClassifyServiceImpl implements ITbClassifyService 
{
    @Autowired
    private TbClassifyMapper tbClassifyMapper;

    /**
     * 查询分类id
分类名称
     * 
     * @param classifyId 分类id
分类名称ID
     * @return 分类id
分类名称
     */
    @Override
    public TbClassify selectTbClassifyById(Long classifyId)
    {
        return tbClassifyMapper.selectTbClassifyById(classifyId);
    }

    /**
     * 查询分类id
分类名称列表
     * 
     * @param tbClassify 分类id
分类名称
     * @return 分类id
分类名称
     */
    @Override
    public List<TbClassify> selectTbClassifyList(TbClassify tbClassify)
    {
        return tbClassifyMapper.selectTbClassifyList(tbClassify);
    }

    /**
     * 新增分类id
分类名称
     * 
     * @param tbClassify 分类id
分类名称
     * @return 结果
     */
    @Override
    public int insertTbClassify(TbClassify tbClassify)
    {
        return tbClassifyMapper.insertTbClassify(tbClassify);
    }

    /**
     * 修改分类id
分类名称
     * 
     * @param tbClassify 分类id
分类名称
     * @return 结果
     */
    @Override
    public int updateTbClassify(TbClassify tbClassify)
    {
        return tbClassifyMapper.updateTbClassify(tbClassify);
    }

    /**
     * 批量删除分类id
分类名称
     * 
     * @param classifyIds 需要删除的分类id
分类名称ID
     * @return 结果
     */
    @Override
    public int deleteTbClassifyByIds(Long[] classifyIds)
    {
        return tbClassifyMapper.deleteTbClassifyByIds(classifyIds);
    }

    /**
     * 删除分类id
分类名称信息
     * 
     * @param classifyId 分类id
分类名称ID
     * @return 结果
     */
    @Override
    public int deleteTbClassifyById(Long classifyId)
    {
        return tbClassifyMapper.deleteTbClassifyById(classifyId);
    }
}
