package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbMaterialMapper;
import com.ruoyi.pos.domain.TbMaterial;
import com.ruoyi.pos.service.ITbMaterialService;

/**
 * 衣服原料
Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
@Service
public class TbMaterialServiceImpl implements ITbMaterialService 
{
    @Autowired
    private TbMaterialMapper tbMaterialMapper;

    /**
     * 查询衣服原料

     * 
     * @param materialId 衣服原料
ID
     * @return 衣服原料

     */
    @Override
    public TbMaterial selectTbMaterialById(Long materialId)
    {
        return tbMaterialMapper.selectTbMaterialById(materialId);
    }

    /**
     * 查询衣服原料
列表
     * 
     * @param tbMaterial 衣服原料

     * @return 衣服原料

     */
    @Override
    public List<TbMaterial> selectTbMaterialList(TbMaterial tbMaterial)
    {
        return tbMaterialMapper.selectTbMaterialList(tbMaterial);
    }

    /**
     * 新增衣服原料

     * 
     * @param tbMaterial 衣服原料

     * @return 结果
     */
    @Override
    public int insertTbMaterial(TbMaterial tbMaterial)
    {
        return tbMaterialMapper.insertTbMaterial(tbMaterial);
    }

    /**
     * 修改衣服原料

     * 
     * @param tbMaterial 衣服原料

     * @return 结果
     */
    @Override
    public int updateTbMaterial(TbMaterial tbMaterial)
    {
        return tbMaterialMapper.updateTbMaterial(tbMaterial);
    }

    /**
     * 批量删除衣服原料

     * 
     * @param materialIds 需要删除的衣服原料
ID
     * @return 结果
     */
    @Override
    public int deleteTbMaterialByIds(Long[] materialIds)
    {
        return tbMaterialMapper.deleteTbMaterialByIds(materialIds);
    }

    /**
     * 删除衣服原料
信息
     * 
     * @param materialId 衣服原料
ID
     * @return 结果
     */
    @Override
    public int deleteTbMaterialById(Long materialId)
    {
        return tbMaterialMapper.deleteTbMaterialById(materialId);
    }
}
