package com.ruoyi.pos.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.pos.domain.TbClothesnum;
import com.ruoyi.pos.domain.TbOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbPaymentMapper;
import com.ruoyi.pos.domain.TbPayment;
import com.ruoyi.pos.service.ITbPaymentService;

/**
 * 订单支付



Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@Service
public class TbPaymentServiceImpl implements ITbPaymentService 
{
    @Autowired
    private TbPaymentMapper tbPaymentMapper;




    /**
     * 查询订单支付




     * 
     * @param id 订单支付



ID
     * @return 订单支付




     */
    @Override
    public TbPayment selectTbPaymentById(Long id)
    {
        return tbPaymentMapper.selectTbPaymentById(id);
    }

    /**
     * 查询订单支付



列表
     * 
     * @param tbPayment 订单支付




     * @return 订单支付




     */
    @Override
    public List<TbPayment> selectTbPaymentList(TbPayment tbPayment)
    {
        return tbPaymentMapper.selectTbPaymentList(tbPayment);
    }

    /**
     * 新增订单支付




     * 
     * @param tbPayment 订单支付




     * @return 结果
     */
    @Override
    public int insertTbPayment(TbPayment tbPayment)
    {


        tbPayment.setPayTime(new Date());


        return tbPaymentMapper.insertTbPayment(tbPayment);
    }

    /**
     * 修改订单支付




     * 
     * @param tbPayment 订单支付




     * @return 结果
     */
    @Override
    public int updateTbPayment(TbPayment tbPayment)
    {
        return tbPaymentMapper.updateTbPayment(tbPayment);
    }

    /**
     * 批量删除订单支付




     * 
     * @param ids 需要删除的订单支付



ID
     * @return 结果
     */
    @Override
    public int deleteTbPaymentByIds(Long[] ids)
    {
        return tbPaymentMapper.deleteTbPaymentByIds(ids);
    }

    /**
     * 删除订单支付



信息
     * 
     * @param id 订单支付



ID
     * @return 结果
     */
    @Override
    public int deleteTbPaymentById(Long id)
    {
        return tbPaymentMapper.deleteTbPaymentById(id);
    }
}
