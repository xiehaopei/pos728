package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbSize;

/**
 * 尺寸Service接口
 * 
 * @author ruoyi
<<<<<<< HEAD
 * @date 2021-05-31
=======
 * @date 2021-06-01
>>>>>>> service
 */
public interface ITbSizeService 
{
    /**
     * 查询尺寸
     * 
     * @param sizeId 尺寸ID
     * @return 尺寸
     */
    public TbSize selectTbSizeById(Long sizeId);

    /**
     * 查询尺寸列表
     * 
     * @param tbSize 尺寸
     * @return 尺寸集合
     */
    public List<TbSize> selectTbSizeList(TbSize tbSize);

    /**
     * 新增尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    public int insertTbSize(TbSize tbSize);

    /**
     * 修改尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    public int updateTbSize(TbSize tbSize);

    /**
     * 批量删除尺寸
     * 
     * @param sizeIds 需要删除的尺寸ID
     * @return 结果
     */
    public int deleteTbSizeByIds(Long[] sizeIds);

    /**
     * 删除尺寸信息
     * 
     * @param sizeId 尺寸ID
     * @return 结果
     */
    public int deleteTbSizeById(Long sizeId);
}
