package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbColor;

/**
 * 衣服颜色Service接口
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
public interface ITbColorService 
{
    /**
     * 查询衣服颜色
     * 
     * @param colorId 衣服颜色ID
     * @return 衣服颜色
     */
    public TbColor selectTbColorById(Long colorId);

    /**
     * 查询衣服颜色列表
     * 
     * @param tbColor 衣服颜色
     * @return 衣服颜色集合
     */
    public List<TbColor> selectTbColorList(TbColor tbColor);

    /**
     * 新增衣服颜色
     * 
     * @param tbColor 衣服颜色
     * @return 结果
     */
    public int insertTbColor(TbColor tbColor);

    /**
     * 修改衣服颜色
     * 
     * @param tbColor 衣服颜色
     * @return 结果
     */
    public int updateTbColor(TbColor tbColor);

    /**
     * 批量删除衣服颜色
     * 
     * @param colorIds 需要删除的衣服颜色ID
     * @return 结果
     */
    public int deleteTbColorByIds(Long[] colorIds);

    /**
     * 删除衣服颜色信息
     * 
     * @param colorId 衣服颜色ID
     * @return 结果
     */
    public int deleteTbColorById(Long colorId);
}
