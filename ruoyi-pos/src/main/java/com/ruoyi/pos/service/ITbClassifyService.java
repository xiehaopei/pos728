package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbClassify;

/**
 * 分类id
分类名称Service接口
 * 
 * @author zsw
 * @date 2021-05-26
 */
public interface ITbClassifyService 
{
    /**
     * 查询分类id
分类名称
     * 
     * @param classifyId 分类id
分类名称ID
     * @return 分类id
分类名称
     */
    public TbClassify selectTbClassifyById(Long classifyId);

    /**
     * 查询分类id
分类名称列表
     * 
     * @param tbClassify 分类id
分类名称
     * @return 分类id
分类名称集合
     */
    public List<TbClassify> selectTbClassifyList(TbClassify tbClassify);

    /**
     * 新增分类id
分类名称
     * 
     * @param tbClassify 分类id
分类名称
     * @return 结果
     */
    public int insertTbClassify(TbClassify tbClassify);

    /**
     * 修改分类id
分类名称
     * 
     * @param tbClassify 分类id
分类名称
     * @return 结果
     */
    public int updateTbClassify(TbClassify tbClassify);

    /**
     * 批量删除分类id
分类名称
     * 
     * @param classifyIds 需要删除的分类id
分类名称ID
     * @return 结果
     */
    public int deleteTbClassifyByIds(Long[] classifyIds);

    /**
     * 删除分类id
分类名称信息
     * 
     * @param classifyId 分类id
分类名称ID
     * @return 结果
     */
    public int deleteTbClassifyById(Long classifyId);
}
