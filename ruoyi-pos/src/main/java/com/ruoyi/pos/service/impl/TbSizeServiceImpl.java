package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbSizeMapper;
import com.ruoyi.pos.domain.TbSize;
import com.ruoyi.pos.service.ITbSizeService;

/**
 * 尺寸Service业务层处理
 * 
 * @author ruoyi
<<<<<<< HEAD
 * @date 2021-05-31
=======
 * @date 2021-06-01
>>>>>>> service
 */
@Service
public class TbSizeServiceImpl implements ITbSizeService 
{
    @Autowired
    private TbSizeMapper tbSizeMapper;

    /**
     * 查询尺寸
     * 
     * @param sizeId 尺寸ID
     * @return 尺寸
     */
    @Override
    public TbSize selectTbSizeById(Long sizeId)
    {
        return tbSizeMapper.selectTbSizeById(sizeId);
    }

    /**
     * 查询尺寸列表
     * 
     * @param tbSize 尺寸
     * @return 尺寸
     */
    @Override
    public List<TbSize> selectTbSizeList(TbSize tbSize)
    {
        return tbSizeMapper.selectTbSizeList(tbSize);
    }

    /**
     * 新增尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    @Override
    public int insertTbSize(TbSize tbSize)
    {
        return tbSizeMapper.insertTbSize(tbSize);
    }

    /**
     * 修改尺寸
     * 
     * @param tbSize 尺寸
     * @return 结果
     */
    @Override
    public int updateTbSize(TbSize tbSize)
    {
        return tbSizeMapper.updateTbSize(tbSize);
    }

    /**
     * 批量删除尺寸
     * 
     * @param sizeIds 需要删除的尺寸ID
     * @return 结果
     */
    @Override
    public int deleteTbSizeByIds(Long[] sizeIds)
    {
        return tbSizeMapper.deleteTbSizeByIds(sizeIds);
    }

    /**
     * 删除尺寸信息
     * 
     * @param sizeId 尺寸ID
     * @return 结果
     */
    @Override
    public int deleteTbSizeById(Long sizeId)
    {
        return tbSizeMapper.deleteTbSizeById(sizeId);
    }
}
