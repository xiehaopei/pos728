package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbColorMapper;
import com.ruoyi.pos.domain.TbColor;
import com.ruoyi.pos.service.ITbColorService;

/**
 * 衣服颜色Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
@Service
public class TbColorServiceImpl implements ITbColorService 
{
    @Autowired
    private TbColorMapper tbColorMapper;

    /**
     * 查询衣服颜色
     * 
     * @param colorId 衣服颜色ID
     * @return 衣服颜色
     */
    @Override
    public TbColor selectTbColorById(Long colorId)
    {
        return tbColorMapper.selectTbColorById(colorId);
    }

    /**
     * 查询衣服颜色列表
     * 
     * @param tbColor 衣服颜色
     * @return 衣服颜色
     */
    @Override
    public List<TbColor> selectTbColorList(TbColor tbColor)
    {
        return tbColorMapper.selectTbColorList(tbColor);
    }

    /**
     * 新增衣服颜色
     * 
     * @param tbColor 衣服颜色
     * @return 结果
     */
    @Override
    public int insertTbColor(TbColor tbColor)
    {
        return tbColorMapper.insertTbColor(tbColor);
    }

    /**
     * 修改衣服颜色
     * 
     * @param tbColor 衣服颜色
     * @return 结果
     */
    @Override
    public int updateTbColor(TbColor tbColor)
    {
        return tbColorMapper.updateTbColor(tbColor);
    }

    /**
     * 批量删除衣服颜色
     * 
     * @param colorIds 需要删除的衣服颜色ID
     * @return 结果
     */
    @Override
    public int deleteTbColorByIds(Long[] colorIds)
    {
        return tbColorMapper.deleteTbColorByIds(colorIds);
    }

    /**
     * 删除衣服颜色信息
     * 
     * @param colorId 衣服颜色ID
     * @return 结果
     */
    @Override
    public int deleteTbColorById(Long colorId)
    {
        return tbColorMapper.deleteTbColorById(colorId);
    }
}
