package com.ruoyi.pos.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pos.mapper.TbOrderidMapper;
import com.ruoyi.pos.domain.TbOrderid;
import com.ruoyi.pos.service.ITbOrderidService;

/**
 * 订单顾客
Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
@Service
public class TbOrderidServiceImpl implements ITbOrderidService 
{
    @Autowired
    private TbOrderidMapper tbOrderidMapper;

    /**
     * 查询订单顾客

     * 
     * @param ordId 订单顾客
ID
     * @return 订单顾客

     */
    @Override
    public TbOrderid selectTbOrderidById(Long ordId)
    {
        return tbOrderidMapper.selectTbOrderidById(ordId);
    }

    /**
     * 查询订单顾客
列表
     * 
     * @param tbOrderid 订单顾客

     * @return 订单顾客

     */
    @Override
    public List<TbOrderid> selectTbOrderidList(TbOrderid tbOrderid)
    {
        return tbOrderidMapper.selectTbOrderidList(tbOrderid);
    }

    /**
     * 新增订单顾客

     * 
     * @param tbOrderid 订单顾客

     * @return 结果
     */
    @Override
    public int insertTbOrderid(TbOrderid tbOrderid)
    {
        return tbOrderidMapper.insertTbOrderid(tbOrderid);
    }

    /**
     * 修改订单顾客

     * 
     * @param tbOrderid 订单顾客

     * @return 结果
     */
    @Override
    public int updateTbOrderid(TbOrderid tbOrderid)
    {
        return tbOrderidMapper.updateTbOrderid(tbOrderid);
    }

    /**
     * 批量删除订单顾客

     * 
     * @param ordIds 需要删除的订单顾客
ID
     * @return 结果
     */
    @Override
    public int deleteTbOrderidByIds(Long[] ordIds)
    {
        return tbOrderidMapper.deleteTbOrderidByIds(ordIds);
    }

    /**
     * 删除订单顾客
信息
     * 
     * @param ordId 订单顾客
ID
     * @return 结果
     */
    @Override
    public int deleteTbOrderidById(Long ordId)
    {
        return tbOrderidMapper.deleteTbOrderidById(ordId);
    }
}
