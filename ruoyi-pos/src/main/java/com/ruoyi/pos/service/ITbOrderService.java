package com.ruoyi.pos.service;

import java.util.List;
import com.ruoyi.pos.domain.TbOrder;

/**
 * 订单信息Service接口
 * 
 * @author ruoyi
 * @date 2021-06-14
 */
public interface ITbOrderService 
{
    /**
     * 查询订单信息
     * 
     * @param ordId 订单信息ID
     * @return 订单信息
     */
    public TbOrder selectTbOrderById(Long ordId);

    /**
     * 查询订单信息列表
     * 
     * @param tbOrder 订单信息
     * @return 订单信息集合
     */
    public List<TbOrder> selectTbOrderList(TbOrder tbOrder);

    /**
     * 新增订单信息
     * 
     * @param tbOrder 订单信息
     * @return 结果
     */
    public int insertTbOrder(TbOrder tbOrder);

    /**
     * 修改订单信息
     * 
     * @param tbOrder 订单信息
     * @return 结果
     */
    public int updateTbOrder(TbOrder tbOrder);

    /**
     * 批量删除订单信息
     * 
     * @param ordIds 需要删除的订单信息ID
     * @return 结果
     */
    public int deleteTbOrderByIds(Long[] ordIds);

    /**
     * 删除订单信息信息
     * 
     * @param ordId 订单信息ID
     * @return 结果
     */
    public int deleteTbOrderById(Long ordId);
}
