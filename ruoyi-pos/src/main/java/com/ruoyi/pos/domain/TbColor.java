package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服颜色对象 tb_color
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
public class TbColor extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 颜色id */
    private Long colorId;

    /** 颜色名称 */
    @Excel(name = "颜色名称")
    private String name;

    /** rgb编码 */
    @Excel(name = "rgb编码")
    private String rgb;

    public void setColorId(Long colorId) 
    {
        this.colorId = colorId;
    }

    public Long getColorId() 
    {
        return colorId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setRgb(String rgb) 
    {
        this.rgb = rgb;
    }

    public String getRgb() 
    {
        return rgb;
    }

    @Override
    public String toString() {
        return "TbColor{" +
                "colorId=" + colorId +
                ", name='" + name + '\'' +
                ", rgb='" + rgb + '\'' +
                '}';
    }
}
