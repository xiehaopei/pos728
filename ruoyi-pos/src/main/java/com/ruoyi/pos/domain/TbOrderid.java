package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单顾客
对象 tb_orderid
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public class TbOrderid extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id */
    private Long ordId;

    /** 顾客名 */
    @Excel(name = "顾客名")
    private String customer;

    public void setOrdId(Long ordId) 
    {
        this.ordId = ordId;
    }

    public Long getOrdId() 
    {
        return ordId;
    }
    public void setCustomer(String customer) 
    {
        this.customer = customer;
    }

    public String getCustomer() 
    {
        return customer;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ordId", getOrdId())
            .append("customer", getCustomer())
            .toString();
    }
}
