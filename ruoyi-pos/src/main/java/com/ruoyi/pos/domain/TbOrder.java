package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单信息对象 tb_order
 * 
 * @author ruoyi
 * @date 2021-06-14
 */
public class TbOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单id号 */
    private Long ordId;

    /** 商品列表 */
    @Excel(name = "商品列表")
    private String clothList;

    /** 订单总额 */
    @Excel(name = "订单总额")
    private Long sum;

    /** 顾客姓名 */
    @Excel(name = "顾客姓名")
    private String customer;

    public void setOrdId(Long ordId) 
    {
        this.ordId = ordId;
    }

    public Long getOrdId() 
    {
        return ordId;
    }
    public void setClothList(String clothList) 
    {
        this.clothList = clothList;
    }

    public String getClothList() 
    {
        return clothList;
    }
    public void setSum(Long sum) 
    {
        this.sum = sum;
    }

    public Long getSum() 
    {
        return sum;
    }
    public void setCustomer(String customer) 
    {
        this.customer = customer;
    }

    public String getCustomer() 
    {
        return customer;
    }

    @Override
    public String toString() {
        return "TbOrder{" +
                "ordId=" + ordId +
                ", clothList='" + clothList + '\'' +
                ", sum=" + sum +
                ", customer='" + customer + '\'' +
                '}';
    }
}
