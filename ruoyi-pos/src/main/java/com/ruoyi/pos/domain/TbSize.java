package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 尺寸对象 tb_size
 * 
 * @author ruoyi
<<<<<<< HEAD
 * @date 2021-05-31
=======
 * @date 2021-06-01
>>>>>>> service
 */
public class TbSize extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long sizeId;

    private String clonthSize;

    private String clonthType;

    public void setSizeId(Long sizeId) 
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId() 
    {
        return sizeId;
    }
    public void setClonthSize(String clonthSize) 
    {
        this.clonthSize = clonthSize;
    }

    public String getClonthSize() 
    {
        return clonthSize;
    }
    public void setClonthType(String clonthType) 
    {
        this.clonthType = clonthType;
    }

    public String getClonthType() 
    {
        return clonthType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sizeId", getSizeId())
            .append("clonthSize", getClonthSize())
            .append("clonthType", getClonthType())
            .toString();
    }
}
