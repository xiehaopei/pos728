package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服库存





对象 tb_clothesnum
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
public class TbClothesnum extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 衣服商品id */
    @Excel(name = "衣服商品id")
    private Long clothId;

    /** 颜色id */
    @Excel(name = "颜色id")
    private Long colorId;

    /** 尺寸id */
    @Excel(name = "尺寸id")
    private Long sizeId;

    /** 门店id */
    @Excel(name = "门店id")
    private Long shopId;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long warehouseId;

    /** 库存数量 */
    private String nums;

    /**连接其他表*/
    private TbClothesid clothes;
    private TbColor color;
    private TbSize size;
    private TbShops shop;
    private TbWarehouse warehouse;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setClothId(Long clothId) 
    {
        this.clothId = clothId;
    }

    public Long getClothId() 
    {
        return clothId;
    }
    public void setColorId(Long colorId) 
    {
        this.colorId = colorId;
    }

    public Long getColorId() 
    {
        return colorId;
    }
    public void setSizeId(Long sizeId) 
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId() 
    {
        return sizeId;
    }
    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setWarehouseId(Long warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public Long getWarehouseId() 
    {
        return warehouseId;
    }
    public void setNums(String nums) 
    {
        this.nums = nums;
    }

    public String getNums() 
    {
        return nums;
    }
    /**商品表*/
    public TbClothesid getClothes() {
        return clothes;
    }

    public void setClothes(TbClothesid clothes) {
        this.clothes = clothes;
    }

    /**颜色表*/
    public TbColor getColor() {
        return color;
    }

    public void setColor(TbColor color) {
        this.color = color;
    }
    /**尺寸表*/
    public TbSize getSize() {
        return size;
    }

    public void setSize(TbSize size) {
        this.size = size;
    }
    /**商铺表*/
    public TbShops getShop() {
        return shop;
    }

    public void setShop(TbShops shop) {
        this.shop = shop;
    }
    /**仓库表*/
    public TbWarehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(TbWarehouse warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public String toString() {
        return "TbClothesnum{" +
                "id=" + id +
                ", clothId=" + clothId +
                ", colorId=" + colorId +
                ", sizeId=" + sizeId +
                ", shopId=" + shopId +
                ", warehouseId=" + warehouseId +
                ", clothes=" + clothes +
                ", color=" + color +
                ", size=" + size +
                ", shop=" + shop +
                ", warehouse=" + warehouse +
                ", nums='" + nums + '\'' +
                '}';
    }
}
