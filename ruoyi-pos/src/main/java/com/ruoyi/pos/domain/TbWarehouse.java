package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服仓库对象 tb_warehouse
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public class TbWarehouse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库id
 */
    private String warehouseId;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String name;

    /** 负责人 */
    @Excel(name = "负责人")
    private String principal;

    public void setWarehouseId(String warehouseId) 
    {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseId() 
    {
        return warehouseId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrincipal(String principal) 
    {
        this.principal = principal;
    }

    public String getPrincipal() 
    {
        return principal;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("warehouseId", getWarehouseId())
            .append("name", getName())
            .append("principal", getPrincipal())
            .toString();
    }
}
