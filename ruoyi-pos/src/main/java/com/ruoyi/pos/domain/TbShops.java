package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 商铺信息对象 tb_shops
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public class TbShops extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 店铺ID */
    private String shopId;

    /** 店铺名 */
    @Excel(name = "店铺名")
    private String name;

    /** 负责人 */
    @Excel(name = "负责人")
    private String principal;

    public void setShopId(String shopId) 
    {
        this.shopId = shopId;
    }

    public String getShopId() 
    {
        return shopId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPrincipal(String principal) 
    {
        this.principal = principal;
    }

    public String getPrincipal() 
    {
        return principal;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shopId", getShopId())
            .append("name", getName())
            .append("principal", getPrincipal())
            .toString();
    }
}
