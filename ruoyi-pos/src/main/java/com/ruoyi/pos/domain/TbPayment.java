package com.ruoyi.pos.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单支付



对象 tb_payment
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public class TbPayment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long ordId;

    /** 应付款 */
    @Excel(name = "应付款")
    private BigDecimal payment;

    /** 付款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "付款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date payTime;

    /** 
付款方法 */
    @Excel(name = " 付款方法")
    private String payMethod;

    /** 付款途径（卡号，微信号之类） */
    @Excel(name = "付款途径", readConverterExp = "卡=号，微信号之类")
    private String payPath;

    private TbOrder order;

    public void setId(Long id) 
    {
        this.id = id;
    }
    public Long getId() 
    {
        return id;
    }

    public void setOrdId(Long ordId) 
    {
        this.ordId = ordId;
    }
    public Long getOrdId() 
    {
        return ordId;
    }

    public void setPayment(BigDecimal payment) 
    {
        this.payment = payment;
    }
    public BigDecimal getPayment() 
    {
        return payment;
    }
    public void setPayTime(Date payTime) 
    {
        this.payTime = payTime;
    }
    public Date getPayTime() 
    {
        return payTime;
    }

    public void setPayMethod(String payMethod) 
    {
        this.payMethod = payMethod;
    }
    public String getPayMethod() 
    {
        return payMethod;
    }

    public void setPayPath(String payPath) 
    {
        this.payPath = payPath;
    }
    public String getPayPath() 
    {
        return payPath;
    }
/*连接订单表**/
    public TbOrder getOrder() {
        return order;
    }

    public void setOrder(TbOrder order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "TbPayment{" +
                "id=" + id +
                ", ordId=" + ordId +
                ", payment=" + payment +
                ", payTime=" + payTime +
                ", payMethod='" + payMethod + '\'' +
                ", payPath='" + payPath + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
