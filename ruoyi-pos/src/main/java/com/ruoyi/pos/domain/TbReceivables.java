package com.ruoyi.pos.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收款状况



对象 tb_receivables
 * 
 * @author ruoyi
 * @date 2021-05-31
 */
public class TbReceivables extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单id */
    @Excel(name = "订单id")
    private Long ordId;

    /** 应收账款 */
    @Excel(name = "应收账款")
    private BigDecimal receivables;

    /** 收款时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "收款时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date receTime;

    /** 收款方法 */
    @Excel(name = "收款方法")
    private String receMethod;

    /** 
收款途径（银行卡号，店铺收款微信号等） */
    @Excel(name = "收款途径", readConverterExp = "银行卡号，店铺收款微信号等")
    private String recePath;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrdId(Long ordId) 
    {
        this.ordId = ordId;
    }

    public Long getOrdId() 
    {
        return ordId;
    }
    public void setReceivables(BigDecimal receivables) 
    {
        this.receivables = receivables;
    }

    public BigDecimal getReceivables() 
    {
        return receivables;
    }
    public void setReceTime(Date receTime) 
    {
        this.receTime = receTime;
    }

    public Date getReceTime() 
    {
        return receTime;
    }
    public void setReceMethod(String receMethod) 
    {
        this.receMethod = receMethod;
    }

    public String getReceMethod() 
    {
        return receMethod;
    }
    public void setRecePath(String recePath) 
    {
        this.recePath = recePath;
    }

    public String getRecePath() 
    {
        return recePath;
    }

    @Override
    public String toString() {
        return "TbReceivables{" +
                "id=" + id +
                ", ordId=" + ordId +
                ", receivables=" + receivables +
                ", receTime=" + receTime +
                ", receMethod='" + receMethod + '\'' +
                ", recePath='" + recePath + '\'' +
                '}';
    }
}
