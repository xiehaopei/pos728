package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服原料
对象 tb_material
 * 
 * @author ruoyi
 * @date 2021-05-29
 */
public class TbMaterial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 原材料id */
    private Long materialId;

    /** 原材料名称 */
    @Excel(name = "原材料名称")
    private String name;

    public void setMaterialId(Long materialId) 
    {
        this.materialId = materialId;
    }

    public Long getMaterialId() 
    {
        return materialId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("materialId", getMaterialId())
            .append("name", getName())
            .toString();
    }
}
