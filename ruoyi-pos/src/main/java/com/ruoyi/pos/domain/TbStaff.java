package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 服装店工作人员对象 tb_staff
 * 
 * @author ruoyi
 * @date 2021-05-20
 */
public class TbStaff extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工作人员id */
    private String sId;

    /** 工作人员姓名 */
    @Excel(name = "工作人员姓名")
    private String name;

    /** 工作人员手机号 */
    @Excel(name = "工作人员手机号")
    private String phone;

    /** 职位 */
    @Excel(name = "职位")
    private String position;

    public void setsId(String sId) 
    {
        this.sId = sId;
    }

    public String getsId() 
    {
        return sId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sId", getsId())
            .append("name", getName())
            .append("phone", getPhone())
            .append("position", getPosition())
            .toString();
    }
}
