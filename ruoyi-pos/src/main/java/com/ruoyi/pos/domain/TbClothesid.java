package com.ruoyi.pos.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服关系
对象 tb_clothesid
 * 
 * @author ruoyi
 * @date 2021-06-05
 */
public class TbClothesid extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 衣服商品id */
    private Long clothId;

    /** 衣服商品名称 */
    @Excel(name = "衣服商品名称")
    private String name;

    public void setClothId(Long clothId) 
    {
        this.clothId = clothId;
    }

    public Long getClothId() 
    {
        return clothId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("clothId", getClothId())
            .append("name", getName())
            .toString();
    }
}
