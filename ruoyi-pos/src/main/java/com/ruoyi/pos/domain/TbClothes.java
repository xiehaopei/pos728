package com.ruoyi.pos.domain;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 衣服商品






对象 tb_clothes
 * 
 * @author ruoyi
 * @date 2021-06-04
 */
public class TbClothes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 衣服名称 */
    @Excel(name = "衣服名称")
    private String name;

    /** 衣服商品id */
    @Excel(name = "衣服商品id")
    private Long clothId;

    /** 颜色id */
    @Excel(name = "颜色id")
    private Long colorId;


    /** 尺寸id */
    @Excel(name = "尺寸id")
    private Long sizeId;

    /** 单价 */
    @Excel(name = "单价")
    private BigDecimal price;

    /** 原料id */
    @Excel(name = "原料id")
    private Long materialId;

    /** 分类id */
    @Excel(name = "分类id")
    private Long classifyId;

    /**
     * 分类菜单
     */
    /** 颜色表 */
    private TbColor color;

    /** 尺寸表 */
    private TbSize size;
    /**分类表*/
    private TbClassify classify;
    /**原料表*/
    private TbMaterial material;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setClothId(Long clothId) 
    {
        this.clothId = clothId;
    }

    public Long getClothId() 
    {
        return clothId;
    }
    public void setColorId(Long colorId) 
    {
        this.colorId = colorId;
    }

    public Long getColorId() 
    {
        return colorId;
    }
    public void setSizeId(Long sizeId) 
    {
        this.sizeId = sizeId;
    }

    public Long getSizeId() 
    {
        return sizeId;
    }
    public void setPrice(BigDecimal price) 
    {
        this.price = price;
    }

    public BigDecimal getPrice() 
    {
        return price;
    }
    public void setMaterialId(Long materialId) 
    {
        this.materialId = materialId;
    }

    public Long getMaterialId() 
    {
        return materialId;
    }
    public void setClassifyId(Long classifyId) 
    {
        this.classifyId = classifyId;
    }

    public Long getClassifyId() 
    {
        return classifyId;
    }

    /**颜色表*/
    public TbColor getColor() {
        return color;
    }

    public void setColor(TbColor color) {
        this.color = color;
    }



    /**尺码表*/
    public TbSize getSize() {
        return size;
    }

    public void setSize(TbSize size) {
        this.size = size;
    }

    /**分类表*/
    public TbClassify getClassify() {
        return classify;
    }

    public void setClassify(TbClassify classify) {
        this.classify = classify;
    }


    /**原料表*/
    public TbMaterial getMaterial() {
        return material;
    }

    public void setMaterial(TbMaterial material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "TbClothes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", clothId=" + clothId +
                ", colorId=" + colorId +
                ", sizeId=" + sizeId +
                ", price=" + price +
                ", materialId=" + materialId +
                ", classifyId=" + classifyId +
                ", color=" + color +
                ", size=" + size +
                ", classify=" + classify +
                ", material=" + material +
                '}';
    }
}
